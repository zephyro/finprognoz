<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_two">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <a href="#" class="header__enter">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__lock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Войти</span>
                            </a>
                        </div>
                    </div>
                </div>
            </header>


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Разместить прогноз</span></li>
                    </ul>
                    <h1 class="color_blue">
                        Ваши прогнозы увидят огромное количество пользователей<br/>
                        Делайте точные прогнозы и это принесет вам существенный доход
                    </h1>

                    <div class="box">
                        <div class="box__header mb_15">
                            <div class="box__header_title">Разместить прогноз</div>
                        </div>
                        <div class="box__content">
                            <div class="box__text bx_md mb_18">
                                Вы можете опубликовать несколько прогнозов. В бесплатном доступе для пользователей ваш прогнозх цены, S/L и комментарии будут скрыты. Укажите стоимость, по которой вы готовы продать их пользователям<br/>
                                Уровень S/L и Комментарии к прогнозу указывать не обязательно.
                            </div>

                            <div class="filter">
                                <div class="filter__row mb_05">
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Рынок</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content" style="max-height: 210px;">
                                                    <ul class="select__option">
                                                        <li><span>Рынок</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Биржа</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <ul class="select__option">
                                                        <li><span>Биржа</span></li>
                                                        <li><span>Длинное название</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_sm">
                                        <div class="select">
                                            <div class="select__label">
                                                <span>Символ</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <div class="select__search">
                                                        <input type="text" class="select__search_input" value="" placeholder="Введите имя аналитика">
                                                        <button type="button" class="select__search_button">
                                                            <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <ul class="select__list">
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Газпром">
                                                                <span>Газпром</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="select__buttons select__buttons_inline">
                                                    <li><button type="button" class="btn btn_green btn_sm select_submit">Выбрать</button></li>
                                                    <li><button type="button" class="btn_clear select_clear">Очистить</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_float">
                                        <ul class="btn_inline">
                                            <li>
                                                <button type="button" class="btn btn_green btn_disable">Выбрать</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_clear">Очистить</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="table_responsive_lg mb_25">
                                <table class="table table_compact">
                                    <thead>
                                    <tr>
                                        <th>Символ</th>
                                        <th class="text_center">Прогноз на дату</th>
                                        <th class="text_center">Диапазон <br/>отклонения даты</th>
                                        <th class="text_center">Цена открытия</th>
                                        <th class="text_center">Ваш прогноз</th>
                                        <th class="text_center">S/L</th>
                                        <th class="text_center">Комментарий</th>
                                        <th class="text_center">Стоимость прогноза</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><span><strong>Лукойл</strong></span></td>
                                        <td>
                                            <div class="form_xxs">
                                                <input type="text" name="date" class="form_control form_date" value="06.12.2019">
                                            </div>
                                        </td>
                                        <td class="text_center text_nowrap"><span>5.11.2019 - 25.11.2019</span></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">
                                            <a href="#new" class="btn_add btn_modal">
                                                <i></i>
                                                <span>Добавить <br/>комментарий</span>
                                            </a>
                                        </td>
                                        <td class="text_center">
                                            <div class="form_xs">
                                                <div class="select">
                                                    <input type="hidden" name="select01" value="250 руб">
                                                    <div class="select__label text_right">
                                                        <span>250 руб</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="select__dropdown">
                                                        <div class="select__content" style="max-height: 90px">
                                                            <ul class="select__option text_right">
                                                                <li><span>250 руб</span></li>
                                                                <li><span>500 руб</span></li>
                                                                <li><span>1 000 руб</span></li>
                                                                <li><span>100 000 руб</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="inactive">
                                        <td><span><strong>USD/RUB</strong></span></td>
                                        <td>
                                            <div class="form_xxs">
                                                <input type="text" name="date" class="form_control form_date" value="06.12.2019">
                                            </div>
                                        </td>
                                        <td class="text_center text_nowrap"><span>5.11.2019 - 25.11.2019</span></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">
                                            <a href="#new" class="btn_add btn_modal">
                                                <i></i>
                                                <span>Добавить <br/>комментарий</span>
                                            </a>
                                        </td>
                                        <td class="text_center">
                                            <div class="form_xs">
                                                <div class="select select_top">
                                                    <input type="hidden" name="select01" value="250 руб">
                                                    <div class="select__label text_right">
                                                        <span>250 руб</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="select__dropdown">
                                                        <div class="select__content" style="max-height: 90px">
                                                            <ul class="select__option text_right">
                                                                <li><span>250 руб</span></li>
                                                                <li><span>500 руб</span></li>
                                                                <li><span>1 000 руб</span></li>
                                                                <li><span>100 000 руб</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="inactive">
                                        <td><span><strong>Apple</strong></span></td>
                                        <td>
                                            <div class="form_xxs">
                                                <input type="text" name="date" class="form_control form_date" value="06.12.2019">
                                            </div>
                                        </td>
                                        <td class="text_center text_nowrap"><span>5.11.2019 - 25.11.2019</span></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">  <input type="text" name="n1" class="form_control text_right"></td>
                                        <td class="text_center">
                                            <a href="#new" class="btn_add btn_modal">
                                                <i></i>
                                                <span>Добавить <br/>комментарий</span>
                                            </a>
                                        </td>
                                        <td class="text_center">
                                            <div class="form_xs">
                                                <div class="select select_top">
                                                    <input type="hidden" name="select01" value="250 руб">
                                                    <div class="select__label text_right">
                                                        <span>250 руб</span>
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="select__dropdown">
                                                        <div class="select__content" style="max-height: 90px">
                                                            <ul class="select__option text_right">
                                                                <li><span>250 руб</span></li>
                                                                <li><span>500 руб</span></li>
                                                                <li><span>1 000 руб</span></li>
                                                                <li><span>100 000 руб</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="box__links">
                                <div class="box__links_left">
                                    <a href="#" class="btn_add">
                                        <i></i>
                                        <span>Добавить прогноз</span>
                                    </a>
                                </div>
                                <div class="box__links_center">
                                    <button type="button" class="btn btn_green btn_midi">Опубликовать</button>
                                </div>
                                <div class="box__links_right">
                                    <strong><a href="#alert" class="btn_modal">Правила и условия размещения прогнозов</a></strong>
                                    <br/>
                                    <strong><a href="#good" class="btn_modal">Условия оплаты за прогнозы</a></strong>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_15">
                            <div class="box__header_title">Потребность в прогнозах</div>
                        </div>
                        <div class="box__content">
                            <div class="box__text mb_20">
                                Вы можете разместить свой прогноз по любому символу из списка. Символы с наибольшей потребностью в прогнозах указаны ниже.<br/>
                                Если вы не нашли в списке символ, по которому вы можете делать прогнозы, то <strong><a href="#">напишите нам</a></strong>.
                            </div>
                            <div class="filter">
                                <div class="filter__row">
                                    <div class="filter__item filter__item_md">
                                        <div class="select">
                                            <div class="select__label">
                                                <span>Поиск по символу</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content" style="max-height: 150px;">
                                                    <div class="select__search">
                                                        <input type="text" class="select__search_input" value="" placeholder="Введите символ">
                                                        <button type="button" class="select__search_button">
                                                            <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <ul class="select__option">
                                                        <li>
                                                            <span>Лукойл</span>
                                                        </li>
                                                        <li>
                                                            <span>USD/RUB</span>
                                                        </li>
                                                        <li>
                                                            <span>Apple</span>
                                                        </li>
                                                        <li>
                                                            <span>Газпром</span>
                                                        </li>
                                                        <li>
                                                            <span>Nike</span>
                                                        </li>
                                                        <li>
                                                            <span>Лукойл</span>
                                                        </li>
                                                        <li>
                                                            <span>USD/RUB</span>
                                                        </li>
                                                        <li>
                                                            <span>Apple</span>
                                                        </li>
                                                        <li>
                                                            <span>Nike</span>
                                                        </li>
                                                        <li>
                                                            <span>Лукойл</span>
                                                        </li>
                                                        <li>
                                                            <span>USD/RUB</span>
                                                        </li>
                                                        <li>
                                                            <span>Apple</span>
                                                        </li>
                                                        <li>
                                                            <span>Nike</span>
                                                        </li>
                                                        <li>
                                                            <span>Лукойл</span>
                                                        </li>
                                                        <li>
                                                            <span>USD/RUB</span>
                                                        </li>
                                                        <li>
                                                            <span>Apple</span>
                                                        </li>
                                                        <li>
                                                            <span>Nike</span>
                                                        </li>
                                                        <li>
                                                            <span>Лукойл</span>
                                                        </li>
                                                        <li>
                                                            <span>USD/RUB</span>
                                                        </li>
                                                        <li>
                                                            <span>Apple</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_float">
                                        <ul class="btn_inline">
                                            <li>
                                                <button type="button" class="btn_clear">Очистить</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="info">
                                <div class="info__data">
                                    <div class="info__table">
                                        <div class="info__table_row">
                                            <div class="info__table_head">Акции</div>
                                            <div class="info__table_col info__table_red">Лукойл</div>
                                            <div class="info__table_col info__table_red">Газпром</div>
                                            <div class="info__table_col info__table_red">NIKE</div>
                                        </div>
                                        <div class="info__table_row">
                                            <div class="info__table_head">Облигации</div>
                                            <div class="info__table_col info__table_red">Евросоюз</div>
                                            <div class="info__table_col info__table_blue">Россия</div>
                                            <div class="info__table_col"></div>
                                        </div>
                                        <div class="info__table_row">
                                            <div class="info__table_head">Валюты</div>
                                            <div class="info__table_col info__table_blue">EUR/USD</div>
                                            <div class="info__table_col info__table_blue">USD/RUB</div>
                                            <div class="info__table_col info__table_blue">EUR/RUB</div>
                                        </div>
                                        <div class="info__table_row">
                                            <div class="info__table_head">Индексы</div>
                                        </div>
                                        <div class="info__table_row">
                                            <div class="info__table_head">Сырье</div>
                                        </div>
                                        <div class="info__table_row">
                                            <div class="info__table_head">Криптовалюты</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="info__text">
                                    <ul class="info__legend">
                                        <li class="info__legend_red">символы с высокой потребностью в прогнозах</li>
                                        <li class="info__legend_blue">символы со средней потребностью в прогнозах</li>
                                    </ul>
                                    <div>В данной таблице отсутствуют символы, по которым уже есть прогнозы в достаточном количестве, но это не означает, что вы не можете размещать по ним свои прогнозы. Ваши прогнозы могут быть лучше уже размещенных!</div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="subscribe">
                        <div class="subscribe__wrap">
                            <div class="subscribe__title">Оформите подписку Эксперт</div>
                            <div class="subscribe__text">и получите возможность сортировки и фильтрации данных всех таблиц, а также детальную информацию по каждому завершенному прогнозу всех аналитиков. </div>
                            <a href="#" class="btn btn_green">Подписаться</a>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <!-- Разместить прогноз -->
        <div class="hide">
            <div class="modal_new" id="new">
                <div class="modal_new__title">Добавить комментарий к прогнозу</div>
                <form class="form">
                    <div class="form_group">
                        <textarea class="form_main" name="message" placeholder="Ваш комментарий" rows="11"></textarea>
                    </div>
                    <div class="form_group mb_25">
                        <div class="file">
                            <div class="file__name"></div>
                            <label class="file__form">
                                <input type="file" name="file" value="">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 19 18"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__attachment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Прикрепить файл</span>
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn_blue">Добавить</button>
                </form>
            </div>
        </div>


        <!-- Зарегистрируйтесь аналитиком -->
        <div class="hide">
            <div class="modal_alert" id="alert">
                <div class="modal_alert__text">Зарегистрируйтесь аналитиком и получите возможность размещать свои прогнозы</div>
                <a href="#" class="btn btn_blue">Зарегистрироваться аналитиком</a>
            </div>
        </div>

        <!-- Ваш прогноз принят -->
        <div class="hide">
            <div class="modal_alert" id="good">
                <div class="modal_alert__text mb_20">Ваш прогноз принят</div>
                <a href="#" class="btn btn_blue">ОК</a>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
