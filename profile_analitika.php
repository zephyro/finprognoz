<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_two">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <div class="user">
                                <div class="user__label">
                                    <span>Иван Иванов</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__bell" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                                <div class="user__dropdown">
                                    <ul>
                                        <li><a href="#">Ссылка первая</a></li>
                                        <li><a href="#">Ссылка вторая</a></li>
                                        <li><a href="#">Ссылка третья</a></li>
                                        <li><a href="#">Ссылка четвертая</a></li>
                                        <li><a href="#">Выйти</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">
                    <ul class="breadcrumb mb_30">
                        <li><a href="#">Главная</a></li>
                        <li><span>Профиль аналитика</span></li>
                    </ul>

                    <div class="profile">
                        <div class="profile__header">
                            <div class="profile__header_content">
                                <div class="profile__header_title">Огурец</div>
                                <div class="profile__header_meta">
                                    <div class="raty"  data-readOnly="true" data-score="4"></div>
                                    <span>Активность 1 день назад</span>
                                </div>
                            </div>
                            <ul class="profile__header_actions">
                                <li><a href="#" class="btn btn_green">Подписаться  на прогнозы</a></li>
                                <li><a href="#" class="btn btn_blue">Заказать у меня прогноз</a></li>
                                <li><a href="#" class="btn btn_blue">Подключить робота</a></li>
                            </ul>
                        </div>
                        <div class="profile__body">
                            <div class="profile__title">Персональные данные</div>
                            <div class="profile__row">
                                <div class="profile__col profile__col_one">
                                    <div class="profile__item">
                                        <div class="profile__item_label">ID</div>
                                        <div class="profile__item_text">77710832</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Ф.И.О.</div>
                                        <div class="profile__item_text">********************</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">E-mail</div>
                                        <div class="profile__item_text">********************</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Телефон</div>
                                        <div class="profile__item_text">********************</div>
                                    </div>
                                </div>
                                <div class="profile__col profile__col_two">
                                    <div class="profile__item">
                                        <div class="profile__item_label">Возраст</div>
                                        <div class="profile__item_text">30 лет</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Пол</div>
                                        <div class="profile__item_text">Мужской</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Страна</div>
                                        <div class="profile__item_text">Новая Зенландия</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Город</div>
                                        <div class="profile__item_text">Красивый</div>
                                    </div>
                                </div>
                                <div class="profile__col profile__col_three">
                                    <div class="profile__item">
                                        <div class="profile__item_label">Дата регистрации</div>
                                        <div class="profile__item_text">19 декабря 2019</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Опыт на финансовых рынках</div>
                                        <div class="profile__item_text">7 лет</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Дипломы и сертификаты аналитика</div>
                                        <div class="profile__item_text">Диплом НИУ Высшая школа экономики, IIBA, Сертификат НОУ Интуит</div>
                                    </div>
                                </div>
                                <div class="profile__col profile__col_four">
                                    <div class="profile__item">
                                        <div class="profile__item_label">Общее количество прогнозов</div>
                                        <div class="profile__item_text">15</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Количество подписчиков на аналитику</div>
                                        <div class="profile__item_text">20</div>
                                    </div>
                                    <div class="profile__item">
                                        <div class="profile__item_label">Методы анализа рынка</div>
                                        <div class="profile__item_text">Alligator, Lorem, Ipsum, Dolor, Аллигатор, IIBA </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_30">
                            <div class="box__header_title">Все текущие прогнозы</div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive table_responsive_lg  mb_35">
                                <table class="table table_compact">
                                    <thead>
                                    <tr>
                                        <th>Дата и время <br/>установки</th>
                                        <th class="text_center">Рынок (Биржа)</th>
                                        <th class="text_center">Символ</th>
                                        <th class="text_center">Точность прогноза <br/>по символу, %</th>
                                        <th class="text_center">Цена открытия</th>
                                        <th class="text_center">Прогноз</th>
                                        <th class="text_center">Дата завершения <br/>прогноза</th>
                                        <th class="text_center">Стоимость</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center"><strong class="color_green">80</strong></td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">**</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">35</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">30 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center"><strong class="color_green">80</strong></td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">**</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center"><strong class="color_green">80</strong></td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">30 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center"><strong class="color_green">80</strong></td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">**</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">ММББ</td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center"><strong class="color_green">80</strong></td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">30 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="box__summary">
                                <div class="box__summary_price"><span>Итого к оплате:</span> <strong class="color_blue"><span>499</span>  руб</strong></div>
                                <div class="box__summary_btn"><a href="#" class="btn btn_green">Оплатить</a></div>
                            </div>
                            <div class="box__view">
                                <button type="button" class="btn btn_arrow_down">
                                    <span>Посмотреть ещё</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__reload" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_30">
                            <div class="box__header_title">Инвестиционные портфели</div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive table_responsive_lg mb_35">
                                <table class="table table_compact">
                                    <thead>
                                    <tr>
                                        <th>Дата составления</th>
                                        <th class="text_center">Дата окончания</th>
                                        <th class="text_center">Рынок</th>
                                        <th class="text_center">Биржа</th>
                                        <th class="text_center">Количество <br/>символов</th>
                                        <th class="text_center">Прогноз <br/>доходности %</th>
                                        <th class="text_center">Стоимость</th>
                                        <th class="text_center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">Сегодня 10:30</td>
                                        <td class="text_center"><div class="small_line" data-tooltip="Акции, Облигации, Форекс, Nise, ММББ, Облигации, Акции"><span>Акции, Облигации, Форекс, Nise, ММББ, Облигации, Акции</span></div></td>
                                        <td class="text_center"><div class="small_line" data-tooltip="Nise, ММВБ, Форекс, Nise, ММББ, Облигации, Акции"><span>Nise, ММВБ, Форекс</span></div></td>
                                        <td class="text_center">3</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Сегодня 10:30</td>
                                        <td class="text_center">Сегодня 10:30</td>
                                        <td class="text_center"><div class="small_line" data-tooltip="Акции, Облигации, Форекс, Nise, ММББ, Облигации, Акции"><span>Акции, Облигации, Форекс, Nise, ММББ, Облигации, Акции</span></div></td>
                                        <td class="text_center"><div class="small_line" data-tooltip="Nise, ММВБ, Форекс, Nise, ММББ, Облигации, Акции"><span>Nise, ММВБ, Форекс</span></div></td>
                                        <td class="text_center">3</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="box__summary">
                                <div class="box__summary_price"><span>Итого к оплате:</span> <strong class="color_blue">0  руб</strong></div>
                                <div class="box__summary_btn"><a href="#" class="btn btn_green">Оплатить</a></div>
                            </div>
                            <div class="box__view">
                                <button type="button" class="btn btn_arrow_down">
                                    <span>Посмотреть ещё</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__reload" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_25">
                            <div class="box__header_title">Статистика</div>
                        </div>
                        <div class="box__content">
                            <ul class="stat_nav">
                                <li class="active"><a href="#">1 мес</a></li>
                                <li><a href="#">3 мес</a></li>
                                <li><a href="#">6 мес</a></li>
                                <li><a href="#">12 мес</a></li>
                                <li><a href="#">За все время</a></li>
                            </ul>
                            <div class="chart__row">
                                <div class="chart__col">
                                    <div class="chart__title">Количество прогнозов</div>
                                    <div class="chart">
                                        <canvas id="chart01"></canvas>
                                    </div>
                                </div>
                                <div class="chart__col">
                                    <div class="chart__title">Точность прогноза тренда</div>
                                    <div class="chart">
                                        <canvas id="chart02"></canvas>
                                    </div>
                                </div>
                                <div class="chart__col">
                                    <div class="chart__title">Точность прогноза цены</div>
                                    <div class="chart">
                                        <canvas id="chart03"></canvas>
                                    </div>
                                </div>
                                <div class="chart__col">
                                    <div class="chart__title">Доходность, пункты</div>
                                    <div class="chart">
                                        <canvas id="chart04"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header">
                            <div class="box__header_title">Средние значения по Символам</div>
                        </div>
                        <div class="box__content">
                            <div class="filter mb_15">
                                <div class="filter__row">
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Рынок</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content" style="max-height: 210px;">
                                                    <ul class="select__option">
                                                        <li><span>Рынок</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                        <li><span>Лукойл</span></li>
                                                        <li><span>USD/RUB</span></li>
                                                        <li><span>Apple</span></li>
                                                        <li><span>Nike</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Биржа</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <ul class="select__option">
                                                        <li><span>Биржа</span></li>
                                                        <li><span>Длинное название</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_sm">
                                        <div class="select">
                                            <div class="select__label">
                                                <span>Символ</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <div class="select__search">
                                                        <input type="text" class="select__search_input" value="" placeholder="Введите имя аналитика">
                                                        <button type="button" class="select__search_button">
                                                            <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <ul class="select__list">
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Газпром">
                                                                <span>Газпром</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="select__buttons select__buttons_inline">
                                                    <li><button type="button" class="btn btn_green btn_sm select_submit">Выбрать</button></li>
                                                    <li><button type="button" class="btn_clear select_clear">Очистить</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_float">
                                        <ul class="btn_inline">
                                            <li>
                                                <button type="button" class="btn btn_green btn_disable">Показать</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_clear">Очистить</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="sort">
                                <div class="sort__title">Сортировки:</div>
                                <div class="sort__row">
                                    <div class="sort__item">
                                        <span>Точность тренда</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="sort__item">
                                        <span>Точность цены</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="sort__item">
                                        <span>Доходность</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="informers">

                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="informers__item">
                                    <div class="informer">
                                        <div class="informer__header">
                                            <div class="informer__header_title">Лукойл</div>
                                            <div class="informer__header_logo">
                                                <img src="images/logo_lk.png" class="img_fluid" alt="">
                                            </div>
                                        </div>
                                        <div class="informer__body">
                                            <div class="informer__name">Акции ММВБ</div>
                                            <div class="informer__count mb_15"><span>Количество прогнозов:</span> <strong>28</strong></div>
                                            <div class="informer__row mb_0">
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>цены</span>
                                                        <strong>30%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Точность <br/>тренда</span>
                                                        <strong class="color_blue">65%</strong>
                                                    </div>
                                                </div>
                                                <div class="informer__item">
                                                    <div class="informer__item_data">
                                                        <span>Доходность, <br/>пункт/мес</span>
                                                        <strong class="color_blue">99999</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="text_center mb_30">
                                <button type="button" class="btn btn_arrow_down">
                                    <span>Посмотреть ещё</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__reload" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                            </div>

                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_30">
                            <div class="box__header_title">История прогнозов</div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive table_responsive_lg  mb_35">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><span class="table_sort">Символ (Биржа)</span></th>
                                        <th class="text_center"><span class="table_sort">Дата <br/>закрытия</span></th>
                                        <th class="text_center"><span class="table_sort">Цена <br/>открытия</span></th>
                                        <th class="text_center"><span class="table_sort">Прогнозируемая <br/>цена</span></th>
                                        <th class="text_center"><span class="table_sort">Цена <br/>закрытия</span></th>
                                        <th class="text_center"><span class="table_sort">Прогноз тренда<br><small>точность в %</small></span></th>
                                        <th class="text_center"><span class="table_sort">Прогноз цены<br><small>точность в %</small></span></th>
                                        <th class="text_center"><span class="table_sort">Доходность<br><small>пункты в %</small></span></th>
                                        <th class="text_center"><span class="table_sort">Срок <br>прогноза</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,002</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,002</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Лукойл</strong></td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">120</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                        <td class="text_center">Внутри дня</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="text_center mb_30">
                                <button type="button" class="btn btn_arrow_down">
                                    <span>Посмотреть ещё</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__reload" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="subscribe">
                        <div class="subscribe__wrap">
                            <div class="subscribe__title">Оформите подписку Эксперт</div>
                            <div class="subscribe__text">и получите возможность сортировки и фильтрации данных всех таблиц, а также детальную информацию по каждому завершенному прогнозу всех аналитиков. </div>
                            <a href="#" class="btn btn_green">Подписаться</a>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <!-- Разместить прогноз -->
        <div class="hide">
            <div class="modal_new" id="new">
                <div class="modal_new__title">Добавить комментарий к прогнозу</div>
                <form class="form">
                    <div class="form_group">
                        <textarea class="form_main" name="message" placeholder="Ваш комментарий" rows="11"></textarea>
                    </div>
                    <div class="form_group mb_25">
                        <div class="file">
                            <div class="file__name"></div>
                            <label class="file__form">
                                <input type="file" name="file" value="">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 19 18"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__attachment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Прикрепить файл</span>
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn_blue">Добавить</button>
                </form>
            </div>
        </div>


        <!-- Зарегистрируйтесь аналитиком -->
        <div class="hide">
            <div class="modal_alert" id="alert">
                <div class="modal_alert__text">Зарегистрируйтесь аналитиком и получите возможность размещать свои прогнозы</div>
                <a href="#" class="btn btn_blue">Зарегистрироваться аналитиком</a>
            </div>
        </div>

        <!-- Зарегистрируйтесь аналитиком -->
        <div class="hide">
            <div class="modal_alert" id="good">
                <div class="modal_alert__text mb_20">Ваш прогноз принят</div>
                <a href="#" class="btn btn_blue">ОК</a>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>


        <script>
            // Chart 1
            var ctx1 = document.getElementById('chart01').getContext('2d');
            var gradientLine1 = ctx1.createLinearGradient(0, 0, 0, 300);
            gradientLine1.addColorStop(0, 'rgba(2, 136, 209,0.2)');
            gradientLine1.addColorStop(1, 'rgba(2, 136, 209,0.2)');
            var myLineChart1 = new Chart(ctx1, {
                type: 'line',
                data: {
                    labels: ['01.19', '03.19', '05.19', '05.19', '07.19', '09.19', '11.19'],
                    datasets: [{
                        label: '',
                        data: [45, 28, 36, 12, 45, 80, 100],
                        borderColor: [
                            'rgba(2, 136, 209, 1)'
                        ],
                        backgroundColor: gradientLine1,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });

            // Chart 2
            var ctx2 = document.getElementById('chart02').getContext('2d');
            var gradientLine2 = ctx2.createLinearGradient(0, 0, 0, 300);
            gradientLine2.addColorStop(0, 'rgba(2, 136, 209,0.2)');
            gradientLine2.addColorStop(1, 'rgba(2, 136, 209,0.2)');
            var myLineChart2 = new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: ['01.19', '03.19', '05.19', '05.19', '07.19', '09.19', '11.19'],
                    datasets: [{
                        label: '',
                        data: [45, 44, 36, 88, 100, 62, 38],
                        borderColor: [
                            'rgba(2, 136, 209, 1)'
                        ],
                        backgroundColor: gradientLine2,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });

            // Chart 3
            var ctx3 = document.getElementById('chart03').getContext('2d');
            var gradientLine3 = ctx3.createLinearGradient(0, 0, 0, 300);
            gradientLine3.addColorStop(0, 'rgba(2, 136, 209,0.2)');
            gradientLine3.addColorStop(1, 'rgba(2, 136, 209,0.2)');
            var myLineChart3 = new Chart(ctx3, {
                type: 'line',
                data: {
                    labels: ['01.19', '03.19', '05.19', '05.19', '07.19', '09.19', '11.19'],
                    datasets: [{
                        label: '',
                        data: [45, 28, 36, 12, 45, 80, 100],
                        borderColor: [
                            'rgba(2, 136, 209, 1)'
                        ],
                        backgroundColor: gradientLine3,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });

            // Chart 4
            var ctx4 = document.getElementById('chart04').getContext('2d');
            var gradientLine4 = ctx2.createLinearGradient(0, 0, 0, 300);
            gradientLine4.addColorStop(0, 'rgba(2, 136, 209,0.2)');
            gradientLine4.addColorStop(1, 'rgba(2, 136, 209,0.2)');
            var myLineChart4 = new Chart(ctx4, {
                type: 'line',
                data: {
                    labels: ['01.19', '03.19', '05.19', '05.19', '07.19', '09.19', '11.19'],
                    datasets: [{
                        label: '',
                        data: [45, 44, 36, 88, 100, 62, 38],
                        borderColor: [
                            'rgba(2, 136, 209, 1)'
                        ],
                        backgroundColor: gradientLine4,
                    }]
                },

                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        displayColors: false,
                        titleFontSize: 0,
                        titleSpacing: 0,
                        titleMarginBottom: 0,
                        bodySpacing: 12
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                    }
                }
            });

        </script>

    </body>
</html>
