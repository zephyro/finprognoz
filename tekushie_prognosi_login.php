z<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_four">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <div class="user">
                                <div class="user__label"><span>Александр Шевченко</span></div>
                                <div class="user__dropdown">
                                    <ul>
                                        <li><a href="#">Ссылка первая</a></li>
                                        <li><a href="#">Ссылка вторая</a></li>
                                        <li><a href="#">Ссылка третья</a></li>
                                        <li><a href="#">Ссылка четвертая</a></li>
                                        <li><a href="#">Выйти</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Текущие прогнозы</span></li>
                    </ul>
                    <h1 class="color_blue mb_25">Будьте всегда в курсе свежих прогнозов на финансовых рынках</h1>

                    <div class="box">
                        <div class="box__header mb_25">
                            <div class="box__header_title">Ваши подписки</div>
                            <div class="box__header_info">Количество подписок – 25</div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive mb_20">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Аналитик</th>
                                        <th class="text_center">Символ</th>
                                        <th class="text_center">Точность прогноза <br/>по символу, %</th>
                                        <th class="text_center">Цена открытия</th>
                                            <th class="text_center">Прогноз</th>
                                        <th class="text_center">Дата и время <br/>установки</th>
                                        <th class="text_center">Дата завершения <br/>прогноза</th>
                                        <th class="text_center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong><a href="#">Огурец</a></strong></td>
                                            <td class="text_center">80</td>
                                            <td class="text_center">Лукойл</td>
                                            <td class="text_center">130</td>
                                            <td class="text_center">сегодня 10:30</td>
                                            <td class="text_center">25 дек 2018</td>
                                            <td class="text_center">25 дек 2018</td>
                                            <td class="text_center">
                                                <div class="info_link">
                                                    <div class="info_link__item">
                                                        <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                    <div class="info_link__tooltip">
                                                        <div class="info_link__tooltip_wrap">
                                                            <div class="info_link__date">17 ноября 2019</div>
                                                            <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                            <a href="#" class="info_link__file">
                                                                <i>
                                                                    <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                                </i>
                                                                <span>Заголовок документа</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><strong><a href="#">Помидор</a></strong></td>
                                            <td class="text_center">80</td>
                                            <td class="text_center">USD/RUB</td>
                                            <td class="text_center">64</td>
                                            <td class="text_center">сегодня 10:30</td>
                                            <td class="text_center">25 дек 2018</td>
                                            <td class="text_center">25 дек 2018</td>
                                            <td class="text_center">
                                                <div class="info_link">
                                                    <div class="info_link__item">
                                                        <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </div>
                                                    <div class="info_link__tooltip">
                                                        <div class="info_link__tooltip_wrap">
                                                            <div class="info_link__date">17 ноября 2019</div>
                                                            <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                            <a href="#" class="info_link__file">
                                                                <i>
                                                                    <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                                </i>
                                                                <span>Заголовок документа</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mb_30">
                                <button type="button" class="btn btn_green">Добавить аналитика</button>
                            </div>
                        </div>
                    </div>


                    <div class="box">
                        <div class="box__header mb_25">
                            <div class="box__header_title">Текущие прогнозы</div>
                        </div>
                        <div class="box__meta">
                            <div class="box__meta_col">Вы можете выбрать прогнозы по дате или по символу на странице  <a href="#"><strong>Получить прогноз </strong></a></div>
                            <div class="box__meta_col"><strong class="color_blue">Количество текущих прогнозов – 250</strong></div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive_md mb_25">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><span class="table_sort">Аналитик</span></th>
                                        <th class="text_center">Символ</th>
                                        <th class="text_center">Точность прогноза <br/>по символу, %</th>
                                        <th class="text_center">Цена открытия</th>
                                        <th class="text_center">Прогноз</th>
                                        <th class="text_center">Дата и время <br/>установки</th>
                                        <th class="text_center">Дата завершения <br/>прогноза*</th>
                                        <th class="text_center">Стоимость</th>
                                        <th class="text_center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>29499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <a href="#" class="info_link">
                                                <svg class="ico_svg" viewBox="0 0 20 18"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">80</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">***</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">25 дек 2018</td>
                                        <td>
                                            <div class="table_price">
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="chk" value="">
                                                    <span></span>
                                                </label>
                                                <span>499 руб.</span>
                                            </div>
                                        </td>
                                        <td class="text_center">
                                            <div class="info_link">
                                                <div class="info_link__item">
                                                    <svg class="ico_svg" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <div class="info_link__tooltip">
                                                    <div class="info_link__tooltip_wrap">
                                                        <div class="info_link__date">17 ноября 2019</div>
                                                        <div class="info_link__text">Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании форм развития. Повседневная практика показывает, что сложившаяся структура организации позволяет выполнять важные задания по разработке систем массового участия.</div>
                                                        <a href="#" class="info_link__file">
                                                            <i>
                                                                <img src="img/icon__pdf.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Заголовок документа</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="inline_box">
                                <div class="inline_box__right">
                                    <div class="inline_box__total">
                                        <span>Итого к оплате:</span>
                                        <strong>499  руб</strong>
                                    </div>
                                    <div class="inline_box__checkout">
                                        <a href="#" class="btn btn_green">Оплатить</a>
                                    </div>
                                </div>
                                <div class="inline_box__left">
                                    <div class="inline_box__button">
                                        <button type="button" class="btn btn_arrow_down">
                                            <span>Посмотреть ещё</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 16 16"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__reload" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </button>
                                    </div>
                                    <div class="inline_box__text">
                                        <span class="point_lead">*</span> — Перенос прогноза на новую дату. Аналитик имеет право сделать один перенос даты прогноза не более, чем на 1/4 длительности прогноза.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="subscribe">
                        <div class="subscribe__wrap">
                            <div class="subscribe__title">Оформите подписку Эксперт</div>
                            <div class="subscribe__text">и получите возможность сортировки и фильтрации данных всех таблиц, а также детальную информацию по каждому завершенному прогнозу всех аналитиков. </div>
                            <a href="#" class="btn btn_green">Подписаться</a>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
