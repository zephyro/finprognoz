<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_account">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <div class="user">
                                <div class="user__label"><span>Александр Шевченко</span></div>
                                <div class="user__dropdown">
                                    <ul>
                                        <li><a href="#">Ссылка первая</a></li>
                                        <li><a href="#">Ссылка вторая</a></li>
                                        <li><a href="#">Ссылка третья</a></li>
                                        <li><a href="#">Ссылка четвертая</a></li>
                                        <li><a href="#">Выйти</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Личный кабинет</span></li>
                    </ul>

                    <div class="account">
                        <div class="account__header">
                            <span>Личный кабинет</span>
                            <button type="button" class="account__header_nav">
                                <span></span>
                            </button>
                        </div>
                        <div class="account__row mb_40">
                            <div class="account__sidebar">
                                <nav class="account_nav">
                                    <ul class="account_nav__primary">
                                        <li class="open">
                                            <a href="#">
                                                <span>Личная информация</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second" style="display: block;">
                                                <li><a href="#">Персональные данные</a></li>
                                                <li class="active"><a href="#">Регистрация аналитика</a></li>
                                                <li><a href="#">Подключить робота</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Мои заказы</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Прогноз на символ и дату</a></li>
                                                <li><a href="#">Заказ на сигналы для торговли внутри дня (роботом)</a></li>
                                                <li><a href="#">Заказ на инвестиционный портфель</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Мои покупки</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Прогнозы</a></li>
                                                <li><a href="#">Подписки</a></li>
                                                <li><a href="#">Инвестиционные портфели</a></li>
                                                <li><a href="#">Роботы</a></li>
                                                <li><a href="#">Обучение</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Аналитик</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Заказы к исполнению</a></li>
                                                <li><a href="#">Текущие прогнозы</a></li>
                                                <li><a href="#">Подписчики</a></li>
                                                <li><a href="#">Инвестиционный портфель</a></li>
                                                <li><a href="#">Обучение (Не готово)</a></li>
                                                <li><a href="#">Моя страница Аналитика</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Финансы</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Выставленные счета</a></li>
                                                <li><a href="#">Роботы</a></li>
                                                <li><a href="#">Обучение</a></li>
                                                <li><a href="#">Мои продажи</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><span>Выйти</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="account__content">
                                <div class="account__title">Регистрация аналитика</div>
                                <div class="account__content_row mb_20">
                                    <div class="account__content_col">
                                        <div class="account__data">
                                            <label class="account__data_label">ID</label>
                                            <div class="account__data_value">77710832</div>
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Фамилия</label>
                                            <input type="text" class="account__data_input" name="lastName" placeholder="" value="Шевченко">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Имя</label>
                                            <input type="text" class="account__data_input" name="FirstName" placeholder="" value="Александр">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Отчество</label>
                                            <input type="text" class="account__data_input" name="secondName" placeholder="" value="Александрович">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Дата рождения</label>
                                            <input type="text" class="account__data_input input_date" name="date" placeholder="" value="19 декабря 1979">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">E-mail</label>
                                            <input type="text" class="account__data_input" name="email" placeholder="" value="Analitik@gmail.com">
                                        </div>
                                        <div class="account__data">
                                            <div class="account__data_label mb_10">Методы анализа рынка</div>
                                            <div class="account__data_row">
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Фундаментальный анализ</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Moving Average</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Alligator</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Parabolic</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Bollinger</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>ROC</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Fibonacci</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>RSI</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Fractals</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Stochastic</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Ichimoku</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>ZigZag</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>MACD</span>
                                                    </label>
                                                </div>
                                                <div class="account__data_col">
                                                    <label class="form_checkbox">
                                                        <input type="checkbox" name="chk01" value="">
                                                        <span>Другое</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="account__content_col">
                                        <div class="account__data">
                                            <label class="account__data_label mb_05">Пол</label>
                                            <ul class="form_inline">
                                                <li>
                                                    <label class="form_radio">
                                                        <input type="radio" name="rd1" value="Мужской" checked>
                                                        <span>Мужской</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="form_radio">
                                                        <input type="radio" name="rd1" value="Женский">
                                                        <span>Женский</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Телефон</label>
                                            <input type="text" class="account__data_input" name="phone" placeholder="" value="+7 901 578 42 94">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Страна</label>
                                            <input type="text" class="account__data_input" name="country" placeholder="" value="Новая Зенландия">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Город</label>
                                            <input type="text" class="account__data_input" name="city" placeholder="" value="Красивый">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Дипломы и сертификаты аналитика</label>
                                            <input type="text" class="account__data_input" name="docs" placeholder="" value="Диплом НИУ Высшая школа экономики, IIBA">
                                        </div>
                                        <div class="account__data">
                                            <div class="upload">
                                                <div class="upload__files">
                                                </div>
                                                <div class="upload__wrap">
                                                    <label class="upload__form">
                                                        <input type="file" name="file" value="">
                                                        <i>
                                                            <svg class="ico_svg" viewBox="0 0 19 18" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__attachment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                        <span>Прикрепить файл</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="account__content_text mb_20">Для вывода средств за проданные прогнозы, подписки и прочие услуги, пожалуйста, укажите свои паспортные данные:</div>

                                <div class="account__content_row">
                                    <div class="account__content_col">
                                        <div class="account__data">
                                            <label class="account__data_label">Паспорт серия и номер</label>
                                            <input type="text" class="account__data_input" name="docNumber" placeholder="" value="239314334">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Кем выдан</label>
                                            <input type="text" class="account__data_input" name="docPalce" placeholder="" value="Доброй тетей в отделе выдачи паспортов">
                                        </div>
                                    </div>
                                    <div class="account__content_col">
                                        <div class="account__data">
                                            <label class="account__data_label">Дата выдачи</label>
                                            <input type="text" class="account__data_input input_date" name="docDate" placeholder="" value="19 декабря 2019">
                                        </div>
                                        <div class="account__data">
                                            <label class="account__data_label">Код подразделения</label>
                                            <input type="text" class="account__data_input " name="docAddress" placeholder="" value="43324343">
                                        </div>
                                    </div>
                                </div>

                                <div class="account__data">
                                    <label class="account__data_label">Адрес регистрации</label>
                                    <input type="text" class="account__data_input " name="docAddress" placeholder="" value="ул. Суперпрогнозов, оф. 7">
                                </div>
                                <div class="account__data">
                                    <div class="file">
                                        <div class="file__name"></div>
                                        <label class="file__form">
                                            <input type="file" name="file" value="">
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 19 18" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__attachment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                            <span>Прикрепить файл</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="account__data mb_25">
                                    <label class="form_checkbox">
                                        <input type="checkbox" name="chrck" value="" checked>
                                        <span>Я принимаю <a href="#">Правила и условия пользования</a></span>
                                    </label>
                                </div>
                                <button type="button" class="btn btn_blue">Сохранить</button>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>


        <!-- Покупка -->
        <div class="hide">
            <a href="#purchase" class="purchase_open btn_modal"></a>
            <div class="purchase" id="purchase">
                <div class="purchase__title">Прогноз</div>
                <div class="purchase__name">Лукойл</div>
                <div class="purchase__date">на 22.11.2019</div>
                <div class="purchase__price">Цена: <strong>490 руб.</strong></div>
                <div class="text_center">
                    <button type="button" class="btn btn_blue">Купить</button>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- Thanks -->
        <div class="hide">
            <a href="#thanks" class="thanks_open btn_modal"></a>
            <div class="thanks" id="thanks">
                Сообщение успешно отправлено.<br/>
                В ближайшее время мы свяжемся с вами!
            </div>
        </div>
        <!-- -->

        <!-- Callback -->
        <div class="hide">
            <div class="callback" id="callback">
                <div class="callback__title">Заказать звонок</div>
                <form class="form">
                    <div class="mb_20">
                        <div class="form_elem">
                            <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Имя</span>
                        </div>
                    </div>
                    <ul class="callback__row">
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Телефон" checked>
                                <span>Телефон</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="WhatsApp">
                                <span>WhatsApp</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Viber">
                                <span>Viber	</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Telegram">
                                <span>Telegram</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Skype">
                                <span>Skype</span>
                            </label>
                        </li>
                    </ul>
                    <div class="mb_30">
                        <div class="form_elem">
                            <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Телефон</span>
                        </div>
                    </div>
                    <div class="mb_25">
                        <label class="form_checkbox">
                            <input type="checkbox" name="check" checked>
                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                        </label>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_blue">Заказать</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- -->


        <!-- Auth -->
        <div class="hide">
            <div class="auth" id="auth">
                <div class="auth__nav">
                    <a href="#" data-tab=".tab1" class="active">Авторизация</a>
                    <a href="#" data-tab=".tab2">Регистрация</a>
                </div>
                <div class="auth__content">
                    <div class="auth__tab tab1 active">
                        <form class="form">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">E-mail</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <div class="form_elem error">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Пароль</span>
                                    <span class="form_elem__message form_elem__alert">Поле должно быть заполнено</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="check" checked>
                                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="mb_20 text_center">
                                <button type="submit" class="btn btn_blue">Войти</button>
                            </div>
                            <div class="mb_40 text_center">
                                <a href="#"><strong>Забыли пароль?</strong></a>
                            </div>
                        </form>
                        <div class="auth__subtitle"><span>Войти через соц. сети</span></div>
                        <div class="auth__social">
                            <a href="#">
                                <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="auth__tab tab2 ">
                        <div class="auth__reg done">
                            <div class="auth__reg_form">
                                <form class="form">
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="lastName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Фамилия</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="firstName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="secondName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Отчество</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="email" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Email<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Телефон</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="passwordOne" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <div class="form_elem">
                                            <input type="text" name="passwordTwo" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Подтвердить пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="check" checked>
                                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                        </label>
                                    </div>
                                    <div class="text_center">
                                        <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                                    </div>
                                </form>
                            </div>
                            <div class="auth__reg_good">
                                <div class="auth__reg_title">Ваша учетная запись ещё не активна. <br/>Подтвердите свой e-mail.</div>
                                <div class="auth__reg_text">Если у вас нет письма в папке Входящие, то проверьте папку Спам или войдите при помощи</div>
                                <div class="auth__social mb_25">
                                    <a href="#">
                                        <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                                    </a>
                                </div>
                                <div class="auth__subtitle mb_25"><span>или</span></div>
                                <button type="button" class="btn">Отправить ссылку активации ещё раз</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
