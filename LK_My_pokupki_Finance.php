<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_account">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <div class="user">
                                <div class="user__label"><span>Александр Шевченко</span></div>
                                <div class="user__dropdown">
                                    <ul>
                                        <li><a href="#">Ссылка первая</a></li>
                                        <li><a href="#">Ссылка вторая</a></li>
                                        <li><a href="#">Ссылка третья</a></li>
                                        <li><a href="#">Ссылка четвертая</a></li>
                                        <li><a href="#">Выйти</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Личный кабинет</span></li>
                    </ul>

                    <div class="account">
                        <div class="account__header">
                            <span>Личный кабинет</span>
                            <button type="button" class="account__header_nav">
                                <span></span>
                            </button>
                        </div>
                        <div class="account__row mb_25">
                            <div class="account__sidebar">
                                <nav class="account_nav">
                                    <ul class="account_nav__primary">
                                        <li>
                                            <a href="#">
                                                <span>Личная информация</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Персональные данные</a></li>
                                                <li><a href="#">Регистрация аналитика</a></li>
                                                <li><a href="#">Подключить робота</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Мои заказы</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Прогноз на символ и дату</a></li>
                                                <li><a href="#">Заказ на сигналы для торговли внутри дня (роботом)</a></li>
                                                <li><a href="#">Заказ на инвестиционный портфель</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Мои покупки</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Прогнозы</a></li>
                                                <li><a href="#">Подписки</a></li>
                                                <li><a href="#">Инвестиционные портфели</a></li>
                                                <li><a href="#">Роботы</a></li>
                                                <li><a href="#">Обучение</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Аналитик</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second">
                                                <li><a href="#">Заказы к исполнению</a></li>
                                                <li><a href="#">Текущие прогнозы</a></li>
                                                <li><a href="#">Подписчики</a></li>
                                                <li><a href="#">Инвестиционный портфель</a></li>
                                                <li><a href="#">Обучение (Не готово)</a></li>
                                                <li><a href="#">Моя страница Аналитика</a></li>
                                            </ul>
                                        </li>
                                        <li class="open">
                                            <a href="#">
                                                <span>Финансы</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                            <ul class="account_nav__second" style="display: block;">
                                                <li><a href="#">Выставленные счета</a></li>
                                                <li class="active"><a href="#">Роботы</a></li>
                                                <li><a href="#">Обучение</a></li>
                                                <li><a href="#">Мои продажи</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><span>Выйти</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="account__content">

                                <div class="account__title">Роботы</div>

                                <div class="table_responsive_md mb_60">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Название робота</th>
                                            <th class="text_center">Дата подключения</th>
                                            <th class="text_center">Аналитики</th>
                                            <th class="text_center">Срок окончания работы робота</th>
                                            <th class="text_center">Статус</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Активный</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center color_blue"><a href="#">Огурец</a>, <a href="#">Помидор</a>, <a href="#">Тыква</a>, <a href="#">Вишенка</a>...</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center"><strong class="color_green">Активно</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Atlas Unplugged</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center color_blue"><a href="#">Огурец</a></td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center"><strong class="color_blue">Продлено</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Aibo</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center color_blue"><a href="#">Бедная Йена</a></td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center"><strong class="color_red">Завершено</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Promobot</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center color_blue"><a href="#">Огурец</a>, <a href="#">Помидор</a>, <a href="#">Тыква</a></td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center"><strong class="color_blue">Продлено</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Иннокентий</td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center color_blue"><a href="#">Фикус</a>, <a href="#">Нео</a></td>
                                            <td class="text_center">10.12.2019</td>
                                            <td class="text_center"><strong class="color_red">Завершено</strong></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <h3>Расчет торговли робота</h3>

                                <div class="robot">
                                    <ul class="robot__info">
                                        <li>Брокер <strong>Альпари</strong></li>
                                        <li>Номер торгового счета <strong>105438573</strong></li>
                                        <li>Валюта расчета <strong>EUR</strong></li>
                                    </ul>
                                    <div class="table_responsive_md">
                                        <table class="table table_lg table_finished">
                                            <thead>
                                            <tr>
                                                <th>Дата</th>
                                                <th class="text_center">Прибыль на счете</th>
                                                <th class="text_center">Текущие сделки</th>
                                                <th class="text_center">Прибыль</th>
                                                <th class="text_center">Оплаченная прибыль</th>
                                                <th class="text_center">Сумма к оплате</th>
                                                <th class="text_center">Курс</th>
                                                <th class="text_center">Сумма в рублях</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">3750</td>
                                            </tr>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">6540</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    15.11.2019
                                                    <div class="table_summary">Итого:</div>
                                                </td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">
                                                    100
                                                    <div class="table_summary"><strong>1 500</strong></div>
                                                </td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">
                                                    9850
                                                    <div class="table_summary"><strong>17 450</strong></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="account__divider mb_45 pt_40"></div>

                                <div class="robot">
                                    <ul class="robot__info">
                                        <li>Брокер <strong>Альпари</strong></li>
                                        <li>Номер торгового счета <strong>105438573</strong></li>
                                        <li>Валюта расчета <strong>EUR</strong></li>
                                    </ul>
                                    <div class="table_responsive_md">
                                        <table class="table table_lg table_finished mb_45">
                                            <thead>
                                            <tr>
                                                <th>Дата</th>
                                                <th class="text_center">Прибыль на счете</th>
                                                <th class="text_center">Текущие сделки</th>
                                                <th class="text_center">Прибыль</th>
                                                <th class="text_center">Оплаченная прибыль</th>
                                                <th class="text_center">Сумма к оплате</th>
                                                <th class="text_center">Курс</th>
                                                <th class="text_center">Сумма в рублях</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">3750</td>
                                            </tr>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">6540</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    15.11.2019
                                                    <div class="table_summary">Итого:</div>
                                                </td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">
                                                    100
                                                    <div class="table_summary"><strong>1 500</strong></div>
                                                </td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">
                                                    9850
                                                    <div class="table_summary"><strong>17 450</strong></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="account__divider mb_45 pt_40"></div>

                                <div class="robot">
                                    <ul class="robot__info mb_30">
                                        <li>Брокер <strong>Альпари</strong></li>
                                        <li>Номер торгового счета <strong>105438573</strong></li>
                                        <li>Валюта расчета <strong>EUR</strong></li>
                                    </ul>
                                    <div class="table_responsive_md">
                                        <table class="table table_lg table_finished mb_45">
                                            <thead>
                                            <tr>
                                                <th>Дата</th>
                                                <th class="text_center">Прибыль на счете</th>
                                                <th class="text_center">Текущие сделки</th>
                                                <th class="text_center">Прибыль</th>
                                                <th class="text_center">Оплаченная прибыль</th>
                                                <th class="text_center">Сумма к оплате</th>
                                                <th class="text_center">Курс</th>
                                                <th class="text_center">Сумма в рублях</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">3750</td>
                                            </tr>
                                            <tr>
                                                <td>15.11.2019</td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">6540</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    15.11.2019
                                                    <div class="table_summary">Итого:</div>
                                                </td>
                                                <td class="text_center">120</td>
                                                <td class="text_center">20</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">100</td>
                                                <td class="text_center">
                                                    100
                                                    <div class="table_summary"><strong>1 500</strong></div>
                                                </td>
                                                <td class="text_center">30</td>
                                                <td class="text_center">
                                                    9850
                                                    <div class="table_summary"><strong>17 450</strong></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="account__divider mb_45 pt_40"></div>

                                <h3>Поступление платежей</h3>
                                <table class="table table_finished table_pay mb_45">
                                    <tr>
                                        <th>Дата</th>
                                        <th class="text_center">Сумма, рублей</th>
                                    </tr>
                                    <tr>
                                        <td>01.01.2020</td>
                                        <td class="text_center">30 000</td>
                                    </tr>
                                    <tr>
                                        <td>01.01.2020</td>
                                        <td class="text_center">10 000</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            01.01.2020
                                            <div class="table_summary">Итого:</div>
                                        </td>
                                        <td class="text_center">
                                            91 000
                                            <div class="table_summary"><strong>91 400 руб.</strong></div>
                                        </td>
                                    </tr>
                                </table>

                                <div class="account__divider mb_35 pt_40"></div>

                                <div class="account_summary">
                                    <ul>
                                        <li><strong>Итого к оплате:</strong></li>
                                        <li><strong>91 400 руб.</strong></li>
                                    </ul>
                                    <ul>
                                        <li><strong>Оплачено:</strong></li>
                                        <li><strong class="color_green">131 200 руб.</strong></li>
                                    </ul>
                                    <ul>
                                        <li><strong>Задолженность:</strong></li>
                                        <li><strong class="color_red">-39 270 руб.</strong></li>
                                    </ul>
                                </div>

                                <button type="button" class="btn btn_green">Оплатить</button>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>


        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
