<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_three">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <a href="#" class="header__enter">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__lock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Войти</span>
                            </a>
                        </div>
                    </div>
                </div>
            </header>


            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Текущие прогнозы</span></li>
                    </ul>
                    <h1 class="color_blue">
                        Мы проанализировали для вас каждый прогноз, измерили их точность и доходность.<br/>
                        Посмотрите кто из аналитиков делает качественные прогнозы
                    </h1>

                    <div class="box">
                        <div class="box__header">
                            <div class="box__header_title">Поиск по завершенным прогнозам</div>
                            <div class="box__header_info">Количество подписок – 250</div>
                        </div>
                        <div class="box__content">

                            <div class="filter">

                                <div class="filter__row">
                                    <div class="filter__item filter__item_xl">
                                        <div class="form_search">
                                            <input type="text" class="form_search__input" value="" placeholder="Введите имя аналитика">
                                            <button type="button" class="form_search__button">
                                                <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter__row">
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Биржа</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <ul class="select__option">
                                                        <li><span>Биржа</span></li>
                                                        <li><span>Длинное название</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <input type="hidden" name="select" value="">
                                            <div class="select__label">
                                                <span>Биржа</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <ul class="select__option">
                                                        <li><span>Биржа</span></li>
                                                        <li><span>Длинное название</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                        <li><span>ММББ</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_sm">
                                        <div class="select">
                                            <div class="select__label">
                                                <span>Символ</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <div class="select__search">
                                                        <input type="text" class="select__search_input" value="" placeholder="Введите имя аналитика">
                                                        <button type="button" class="select__search_button">
                                                            <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <ul class="select__list">
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Газпром">
                                                                <span>Газпром</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Nike">
                                                                <span>Nike</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>Лукойл</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="USD/RUB">
                                                                <span>USD/RUB</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Apple">
                                                                <span>Apple</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="select__buttons select__buttons_inline">
                                                    <li><button type="button" class="btn btn_green btn_sm select_submit">Выбрать</button></li>
                                                    <li><button type="button" class="btn_clear select_clear">Очистить</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_xs">
                                        <div class="select">
                                            <div class="select__label">
                                                <span>Статус</span>
                                                <i>
                                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </div>
                                            <div class="select__dropdown">
                                                <div class="select__content">
                                                    <ul class="select__list">
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>
                                                                    <div class="raty"  data-readOnly="true" data-score="5"></div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>
                                                                    <div class="raty"  data-readOnly="true" data-score="4"></div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>
                                                                    <div class="raty"  data-readOnly="true" data-score="3"></div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>
                                                                       <div class="raty"  data-readOnly="true" data-score="2"></div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="form_checkbox">
                                                                <input type="checkbox" name="n1" value="Лукойл">
                                                                <span>
                                                                    <div class="raty"  data-readOnly="true" data-score="1"></div>
                                                                </span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="select__buttons">
                                                    <li><button type="button" class="btn btn_green btn_sm select_submit">Выбрать</button></li>
                                                    <li><button type="button" class="btn_clear select_clear">Очистить</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter__item filter__item_float">
                                        <ul class="btn_inline">
                                            <li>
                                                <button type="button" class="btn btn_green btn_disable">Показать</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_clear">Очистить</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="selected">
                                <div class="selected__title">Выбрано:</div>
                                <div class="selected__row">
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Газпром</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">EUR/USD</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Nike</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Газпром</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Газпром</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Газпром</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">EUR/USD</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Nike</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Лукойл</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                        <span class="selected__item_name">Газпром</span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                    <span class="selected__item_image">
                                        <img src="img/star_five.svg" alt="">
                                    </span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="selected__item">
                                    <span class="selected__item_image">
                                        <img src="img/star_three.svg" alt="">
                                    </span>
                                        <i class="selected__item_remove">
                                            <svg class="ico_svg" viewBox="0 0 8 8"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <button type="button" class="btn_clear btn_clear_blue">Очистить фильтры</button>
                                </div>
                            </div>

                            <div class="sort">
                                <div class="sort__title">Сортировки:</div>
                                <div class="sort__row">
                                    <div class="sort__item">
                                        <span>Кол-во символов</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="sort__item">
                                        <span>Точность тренда</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="sort__item">
                                        <span>Точность цены</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="sort__item">
                                        <span>Доходность</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 17"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__sort" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>

                            <div class="calendar__group">
                                <div class="calendar__item">
                                    <div class="calendar calendar_static">
                                        <input class="datetimepicker1" type="text" style="display: none;">
                                    </div>
                                </div>
                                <div class="calendar__item">
                                    <div class="calendar">
                                        <input class="datetimepicker2" type="text" style="display: none;">
                                    </div>
                                </div>
                                <div class="calendar__item">
                                    <div class="calendar calendar_static">
                                        <input class="datetimepicker3" type="text" style="display: none;">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="box">
                        <div class="box__header mb_28">
                            <div class="box__header_title">Завершенные прогнозы</div>
                        </div>
                        <div class="box__content">
                            <div class="table_responsive table_blur mb_30">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><span class="table_sort">Аналитик</span></th>
                                        <th class="text_center"><span class="table_sort sort_up">Символ</span></th>
                                        <th class="text_center"><span class="table_sort">Дата открытия</span></th>
                                        <th class="text_center"><span class="table_sort sort_down">Дата закрытия</span></th>
                                        <th class="text_center"><span class="table_sort">Цена закрытия</span></th>
                                        <th class="text_center"><span class="table_sort">Прогноз тренда<br><small>точность в %</small></span></th>
                                        <th class="text_center"><span class="table_sort">Прогноз цены<br><small>точность в %</small></span></th>
                                        <th class="text_center"><span class="table_sort">Доходность<br><small>пункты</small></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <tr>
                                        <td><a href="#"><strong>Огурец</strong></a></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>USD/RUB</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,02</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>USD/RUB</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,02</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>USD/RUB</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,02</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>USD/RUB</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,02</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>USD/RUB</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_red">-0,02</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong><a href="#">Огурец</a></strong></td>
                                        <td class="text_center"><strong>Лукойл</strong></td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">сегодня 10:30</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">130</td>
                                        <td class="text_center">80</td>
                                        <td class="text_center"><strong class="color_green">0,001</strong></td>
                                    </tr>

                                    </tbody>
                                </table>
                                <div class="table_blur__content table_blur_xl">

                                    <div class="table_blur__main">
                                        <div class="table_blur__icon">
                                            <svg class="ico_svg" viewBox="0 0 39 42"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__lock2" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <div class="table_blur__data"><strong>Доступно после бесплатной регистрации</strong></div>
                                        <div class="table_blur__button">
                                            <a href="#" class="btn btn_green">
                                                <span>Бесплатная регистрация</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="subscribe">
                        <div class="subscribe__wrap">
                            <div class="subscribe__title">Оформите подписку Эксперт</div>
                            <div class="subscribe__text">и получите возможность сортировки и фильтрации данных всех таблиц, а также детальную информацию по каждому завершенному прогнозу всех аналитиков. </div>
                            <a href="#" class="btn btn_green">Подписаться</a>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
