<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>

        <div class="page page_content">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#" class="btn">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#" class="btn btn_blue">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <img src="img/logo.svg" class="img_fluid" about="">
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <div class="user">
                                <div class="user__label"><span>Александр Шевченко</span></div>
                                <div class="user__dropdown">
                                    <ul>
                                        <li><a href="#">Ссылка первая</a></li>
                                        <li><a href="#">Ссылка вторая</a></li>
                                        <li><a href="#">Ссылка третья</a></li>
                                        <li><a href="#">Ссылка четвертая</a></li>
                                        <li><a href="#">Выйти</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <section class="main">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li><span>Правила и документы</span></li>
                    </ul>

                    <div class="account">
                        <div class="account__header">
                            <span>Правила и документы</span>
                            <button type="button" class="account__header_nav">
                                <span></span>
                            </button>
                        </div>
                        <div class="account__row mb_40">
                            <div class="account__sidebar">
                                <nav class="account_nav">
                                    <ul class="account_nav__single">
                                        <li><a href="#"><span>Правила и условия пользования</span></a></li>
                                        <li><a href="#"><span>Правила для комментариев к прогнозам</span></a></li>
                                        <li><a href="#"><span>Политика конфиденциальности</span></a></li>
                                        <li><a href="#"><span>Договор оферты</span></a></li>
                                        <li><a href="#"><span>Правила размещения прогнозов</span></a></li>
                                        <li class="active"><a href="#"><span>Условия досрочного завершения прогноза</span></a></li>
                                        <li><a href="#"><span>Договор на торговлю роботом</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="account__content">

                                <div class="account__title">Условия досрочного завершения прогноза</div>
                                <div class="content_text">

                                    <ol>
                                        <li>Ваш прогноз будет автоматически завершен в указанную вами дату. Расчет точности и доходности будет проведет по лучшей для вас цене на эту дату.</li>
                                        <li>Диапазон отклонения даты - это временной диапазон, равный ¼ времени до и после его завершения начиная с даты открытия прогноза. В этом диапазоне вы можете вручную закрыть прогноз по текущей цене без потери точности и доходности прогноза. В случае автопродления прогноза срок его завершения переносится на новую дату, указанную в этом диапазоне.</li>
                                        <li>Автопродление прогноза - это возможность продлить срок прогноза до указанной даты в случае, если цена не достигла указанного уровня в установленный вами срок.</li>
                                        <li>Вы можете Завершить размещенный прогноз вручную раньше установленной вами даты его завершения.</li>
                                        <li>Если вы вручную закрываете прогноз, когда с момента его размещения прошло ¾ времени до его завершения, то расчеты точности и доходности будут проведены в соответствии с текущей ценой завершения.</li>
                                        <li>Если вы закрываете прогноз, когда с момента его размещения не прошло ¾ времени до его завершения, то расчеты точности будут равны 0, а расчет доходности будет проведен в соответствии с текущей ценой завершения.</li>
                                        <li>Если в установленную дату прогноз не достиг прогнозируемой цены, то прогноз может быть автоматически продлен на срок не более ¼ времени длительности прогноза с даты установки. Расчеты точности и доходности будут проведены по лучшей цена на новую дату или на момент его закрытия вручную.</li>
                                    </ol>

                                    <p>Все текущие прогнозы закрыты для Пользователей. При приобретении подписки на прогнозы конкретного аналитика Пользователю открываются все текущие прогнозы аналитика. Информация о составе инвестиционных портфелей аналитика остается закрытой пока пользователь не приобретет выбранный портфель .</p>
                                    <p>Прогнозы носят информационный характер и не должны восприниматься как рекомендации и побуждение к действиям.</p>
                                    <p>Любые сигналы, мнения, предупреждения, оповещения, обещания и т.п. - это только предположения основанные на опыте аналитика.</p>
                                    <p>Сайт и приложение могут раскрывать содержимое платных прогнозов в маркетинговых целях без предварительного уведомления аналитика. Аналитики могут самостоятельно определять стоимость, по которой Если кто-то купил прогноз, то мы платим аналитику.</p>
                                    <p>При опубликовании прогнозов аналитик отдает нам права на его использование.</p>
                                    <p>Если пользователь подписывается на прогнозы Аналитика, то ему открываются все текущие прогнозы Аналитика за исключением тех, которые входят в инвестиционные портфели. Доступ к содержимому инвестиционного портфеля открывается после его покупки.</p>
                                    <p>Компания вправе продавать услуги аналитика с наценкой и со скидкой. Размер скидки определяет Компания без согласования с аналитиком. При продаже услуг со скидкой Аналитику будет произведена оплата также со скидкой.</p>
                                    <p>При возвратах предоплаты клиент получает средства за вычетом всех комиссий на отправку и получение.</p>


                                    <ol class="second_list">
                                        <li>Яндекс.Такси вправе в любое время без уведомления Пользователя изменить положения настоящих Условий, а ООО «ЯНДЕКС» вправе в любое время без уведомления Пользователя изменить условия Регулирующих документов. Действующая редакция настоящих Условий размещаются по адресу: https://yandex.ru/legal/taxi_termsofuse. Риск неознакомления с новой редакцией Условий и Регулирующих документов несет Пользователь, продолжение пользования Сервисом после изменения Условий или Регулирующих документов считается согласием с их новой редакцией.</li>
                                        <li>В случае если Яндексом были внесены какие-либо изменения в настоящие Условия, в порядке, предусмотренном п.1.4. настоящих Условий, с которыми Пользователь не согласен, он обязан прекратить использование Сервиса.</li>
                                        <li>Сервис предлагает Пользователю бесплатную возможность разместить информацию о потенциальном спросе Пользователя на услуги по перевозке пассажиров и багажа легковым такси и/или услуги по управлению транспортным средством Пользователя и/или иные транспортные услуги и/или услуги курьерской доставки, а также возможность ознакомиться с информацией о предложениях организаций, оказывающих услуги в указанной сфере (далее — «партнеры Сервиса» или «Службы Такси») и осуществить поиск таких предложений по заданным Пользователем параметрам. Все существующие на данный момент функции Сервиса, а также любое развитие их и/или добавление новых является предметом настоящих Условий.</li>
                                        <li>Используя Сервис, Пользователь дает свое согласие на получение сообщений рекламного характера. Пользователь вправе отказаться от получения сообщений рекламного характера путем использования соответствующего функционала Сервиса или следуя инструкциям, указанным в полученном сообщении рекламного характера.</li>
                                        <li>Яндекс.Такси и/или привлекаемые третьи лица вправе осуществлять сбор мнений и отзывов Пользователей по различным вопросам работы Сервиса в статистических целях, для контроля качества оказываемых услуг, а также использовать полученные данные в обезличенном виде для работы Сервиса. Опрос Пользователей может проводиться путем направления информационного сообщения либо осуществления связи по контактным данным, указанным Пользователем в учетной записи (посредством телефонных звонков или электронных писем).</li>
                                        <li>Сервис предоставляется Пользователю для личного некоммерческого использования.</li>
                                        <li>Яндекс.Такси не несет ответственности за содержание и/или актуальность информации, предоставляемой Службами Такси, включая информацию о стоимости услуг Служб Такси, а также об их наличии в данный момент. Взаимодействие Пользователя со Службами Такси по вопросам приобретения услуг осуществляется Пользователем самостоятельно (без участия Яндекс.Такси) в соответствии с принятыми у Служб Такси правилами оказания услуг. Яндекс.Такси не несет ответственности за финансовые и любые другие операции, совершаемые Пользователем и Службами Такси, а также за любые последствия приобретения Пользователем услуг Служб Такси.</li>
                                        <li>Пользователь дает Яндекс.Такси согласие на обработку персональной информации (включая персональные данные) Пользователя либо лица, в отношении которого Пользователь размещает посредством Сервиса информацию о потенциальном спросе на услуги перевозки пассажиров и багажа легковым такси и/или на услуги по управлению транспортным средством и/или на иные транспортные услуги и/или услуги курьерской доставки, либо лица, информацию о котором Пользователь указал в соответствии с разделом 5 настоящих Условий или указал в качестве контактного лица (получателя) при оформлении услуги курьерской доставки, на передачу Яндекс.Такси такой персональной информации компаниям, входящим в одну группу лиц с Яндекс.Такси, Службам Такси, а также на обработку такой персональной информации компаниями, входящими в одну группу лиц с Яндекс.Такси, Службами Такси, для целей предоставления Пользователю или указанному Пользователю лицу Сервиса. При использовании Пользователем Сервиса указанная персональная информация передается в ООО «ЯНДЕКС» (ОГРН: 1027700229193), для обработки на условиях и для целей, определённых в Политике конфиденциальности ООО «ЯНДЕКС», доступной для ознакомления по адресу: https://yandex.ru/legal/confidential.</li>
                                        <li>Яндекс.Такси не имеет доступа к указанным Пользователем данным банковской карты и не несет ответственности за сохранность и конфиденциальность передаваемых данных при проведении безналичной оплаты. Безналичная оплата осуществляется Пользователем с участием уполномоченного оператора по приему платежей, или оператора электронных денежных средств, или иных участников расчетов, информационно-технологического взаимодействия, и регулируется правилами международных платежных систем, банков (в том числе банка-эмитента Привязанной карты) и других участников расчетов.</li>
                                        <li>
                                            <p>При указании своих данных согласно п. 3.2 настоящих Условий и дальнейшем использовании Привязанной карты Пользователь подтверждает и гарантирует указание им достоверной и полной информации о действительной банковской карте, выданной на его имя; соблюдение им правил международных платежных систем и требований банка-эмитента, выпустившего Привязанную карту, в том числе в отношении порядка проведения безналичных расчетов.</p>
                                            Правила вывода - минимальная сумма вывода средств составляет 5000 рублей.
                                        </li>

                                    </ol>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <img src="img/logo.svg" class="img_fluid" about="">
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#" class="btn btn_blue">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>


        <!-- Покупка -->
        <div class="hide">
            <a href="#purchase" class="purchase_open btn_modal"></a>
            <div class="purchase" id="purchase">
                <div class="purchase__title">Прогноз</div>
                <div class="purchase__name">Лукойл</div>
                <div class="purchase__date">на 22.11.2019</div>
                <div class="purchase__price">Цена: <strong>490 руб.</strong></div>
                <div class="text_center">
                    <button type="button" class="btn btn_blue">Купить</button>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- Thanks -->
        <div class="hide">
            <a href="#thanks" class="thanks_open btn_modal"></a>
            <div class="thanks" id="thanks">
                Сообщение успешно отправлено.<br/>
                В ближайшее время мы свяжемся с вами!
            </div>
        </div>
        <!-- -->

        <!-- Callback -->
        <div class="hide">
            <div class="callback" id="callback">
                <div class="callback__title">Заказать звонок</div>
                <form class="form">
                    <div class="mb_20">
                        <div class="form_elem">
                            <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Имя</span>
                        </div>
                    </div>
                    <ul class="callback__row">
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Телефон" checked>
                                <span>Телефон</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="WhatsApp">
                                <span>WhatsApp</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Viber">
                                <span>Viber	</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Telegram">
                                <span>Telegram</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Skype">
                                <span>Skype</span>
                            </label>
                        </li>
                    </ul>
                    <div class="mb_30">
                        <div class="form_elem">
                            <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Телефон</span>
                        </div>
                    </div>
                    <div class="mb_25">
                        <label class="form_checkbox">
                            <input type="checkbox" name="check" checked>
                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                        </label>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_blue">Заказать</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- -->


        <!-- Auth -->
        <div class="hide">
            <div class="auth" id="auth">
                <div class="auth__nav">
                    <a href="#" data-tab=".tab1" class="active">Авторизация</a>
                    <a href="#" data-tab=".tab2">Регистрация</a>
                </div>
                <div class="auth__content">
                    <div class="auth__tab tab1 active">
                        <form class="form">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">E-mail</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <div class="form_elem error">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Пароль</span>
                                    <span class="form_elem__message form_elem__alert">Поле должно быть заполнено</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="check" checked>
                                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="mb_20 text_center">
                                <button type="submit" class="btn btn_blue">Войти</button>
                            </div>
                            <div class="mb_40 text_center">
                                <a href="#"><strong>Забыли пароль?</strong></a>
                            </div>
                        </form>
                        <div class="auth__subtitle"><span>Войти через соц. сети</span></div>
                        <div class="auth__social">
                            <a href="#">
                                <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="auth__tab tab2 ">
                        <div class="auth__reg done">
                            <div class="auth__reg_form">
                                <form class="form">
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="lastName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Фамилия</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="firstName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="secondName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Отчество</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="email" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Email<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Телефон</span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="passwordOne" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <div class="form_elem">
                                            <input type="text" name="passwordTwo" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Подтвердить пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="check" checked>
                                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                        </label>
                                    </div>
                                    <div class="text_center">
                                        <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                                    </div>
                                </form>
                            </div>
                            <div class="auth__reg_good">
                                <div class="auth__reg_title">Ваша учетная запись ещё не активна. <br/>Подтвердите свой e-mail.</div>
                                <div class="auth__reg_text">Если у вас нет письма в папке Входящие, то проверьте папку Спам или войдите при помощи</div>
                                <div class="auth__social mb_25">
                                    <a href="#">
                                        <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                                    </a>
                                </div>
                                <div class="auth__subtitle mb_25"><span>или</span></div>
                                <button type="button" class="btn">Отправить ссылку активации ещё раз</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
