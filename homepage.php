<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
        <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
        <link rel="stylesheet" href="css/main.css">

        <style>

        </style>

    </head>
    <body>


        <div class="page page_home">

            <div class="top">
                <div class="container">
                    <div class="top__row">
                        <div class="top__toggle nav_toggle">
                            <span></span>
                        </div>
                        <div class="top__rate">
                            <div class="top__rate_item">
                                <div class="top__rate_name">GOLD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1415,50</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">EURUSD</div>
                                <div class="top__rate_value top__rate_up">
                                    <span>1,101241</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                            <div class="top__rate_item">
                                <div class="top__rate_name">GBPUSD</div>
                                <div class="top__rate_value top__rate_down">
                                    <span>1,28418</span>
                                    <i>
                                        <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="top__content">
                            <div class="lng">
                                <div class="lng__active">
                                    <div class="lng__active_icon">
                                        <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="lng__active_name">Ru</div>
                                    <div class="lng__active_arrow">
                                        <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="lng__content">
                                    <a href="#" class="lng__item">En</a>
                                    <a href="#" class="lng__item">Ru</a>
                                    <a href="#" class="lng__item">De</a>
                                </div>
                            </div>
                            <div class="top__content_item hi">
                                <a href="#reg" class="btn btn_modal">Разместить прогноз</a>
                            </div>
                            <div class="top__content_item">
                                <a href="#auth" class="btn btn_blue btn_modal">Получить прогноз</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <a href="#" class="header__logo">
                            <strong>FIN</strong>
                            <span>Prognoz</span>
                        </a>

                        <nav class="nav">
                            <span class="nav__close nav_toggle"></span>
                            <ul class="nav__menu">
                                <li>
                                    <a href="#">
                                        <span>Прогнозы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                        <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                        <li><a href="#"><span>Заказать прогноз</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Аналитики</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Роботы</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Аренда робота</span></a></li>
                                        <li><a href="#"><span>50/50</span></a></li>
                                        <li><a href="#"><span>Продажа робота</span></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Обучение</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav_blue">
                                        <span>Для Аналитиков</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <b>99</b>
                                    </a>
                                    <ul>
                                        <li><a href="#"><span>Разместить прогноз</span></a></li>
                                        <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                        <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </nav>
                        <div class="nav__layout nav_toggle"></div>
                        <div class="header__auth">
                            <a href="#auth" class="header__enter btn_modal">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 14 14"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__lock" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Войти</span>
                            </a>
                        </div>
                    </div>
                </div>
            </header>

            <section class="primary">
                <div class="primary__wrap">
                    <div class="container">
                        <h1 class="primary__header">Прогнозы на <br/>финансовых рынках</h1>
                        <div class="primary__text">
                            <p>От лучших аналитиков с анализом их точности и доходности.</p>
                            <p>Только цифры, никаких слов и рассуждений!</p>
                        </div>
                        <a href="#reg_base" class="btn btn_md text_uppercase btn_modal">Бесплатная регистрация</a>
                    </div>
                </div>
            </section>

            <section class="home">
                <div class="container">

                    <div class="forecast">
                        <div class="forecast__mobile">
                            <div class="forecast__title">Текущие прогнозы</div>
                            <div class="forecast__text">Без лишних слов - только цифры! Прогнозы от профессионалов по всем рынкам с конкретными ценами и датами.</div>
                        </div>
                        <div class="forecast__content">
                            <div class="forecast__bg" style="background-image: url('images/forecast__01.jpg')"></div>
                            <div class="forecast__wrap">
                                <div class="forecast__desktop">
                                    <div class="forecast__title">Текущие прогнозы</div>
                                    <div class="forecast__text">Без лишних слов - только цифры! Прогнозы от профессионалов по всем рынкам с конкретными ценами и датами.</div>
                                </div>
                                <div class="forecast__data">
                                    <table class="table table_collapse forecast__table">
                                        <thead>
                                        <tr>
                                            <th>Аналитик</th>
                                            <th class="text_center">Символ</th>
                                            <th class="text_center">Точность прогноза <br/>по символу, %</th>
                                            <th class="text_center">Прогноз</th>
                                            <th class="text_center">Дата и время <br/>установки</th>
                                            <th class="text_center">Дата <br/>завершения</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Александр</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Точность прогноза по символу, %" class="text_center">75</td>
                                            <td data-head="Прогноз" class="text_center">****</td>
                                            <td data-head="Дата и время установки" class="text_center text_nowrap">сегодня 10:30</td>
                                            <td data-head="Дата завершения" class="text_center text_nowrap">сегодня</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Точность прогноза по символу, %" class="text_center">75</td>
                                            <td data-head="Прогноз" class="text_center">****</td>
                                            <td data-head="Дата и время установки" class="text_center text_nowrap">сегодня 10:30</td>
                                            <td data-head="Дата завершения" class="text_center text_nowrap">сегодня</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Точность прогноза по символу, %" class="text_center">75</td>
                                            <td data-head="Прогноз" class="text_center">****</td>
                                            <td data-head="Дата и время установки" class="text_center text_nowrap">сегодня 10:30</td>
                                            <td data-head="Дата завершения" class="text_center text_nowrap">сегодня</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Точность прогноза по символу, %" class="text_center">75</td>
                                            <td data-head="Прогноз" class="text_center">****</td>
                                            <td data-head="Дата и время установки" class="text_center text_nowrap">сегодня 10:30</td>
                                            <td data-head="Дата завершения" class="text_center text_nowrap">сегодня</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Точность прогноза по символу, %" class="text_center">75</td>
                                            <td data-head="Прогноз" class="text_center">****</td>
                                            <td data-head="Дата и время установки" class="text_center text_nowrap">сегодня 10:30</td>
                                            <td data-head="Дата завершения" class="text_center text_nowrap">сегодня</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="text_center">
                                        <a href="#" class="btn btn_arrow_right">
                                            <span>Посмотреть ещё</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="gods_row">
                        <div class="goods">
                            <a href="#" class="goods__image">
                                <img src="images/goods__01.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Разместить прогноз</a>
                                <div class="goods__text">Зарабатывайте на своих знаниях, разместив прогноз за 30 секунд! Здесь важны только цифры, а не слова и рассуждения</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="goods">
                            <a href="#" class="goods__image">
                                <img src="images/goods__02.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Заказать прогноз</a>
                                <div class="goods__text">Разместите свой запрос и получите прогноз в кратчайшее время</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="forecast forecast_invert">
                        <div class="forecast__mobile">
                            <div class="forecast__title">Завершенные прогнозы</div>
                            <div class="forecast__text">Мы проанализировали для вас каждый прогноз, измерили их точность и доходность</div>
                        </div>
                        <div class="forecast__content">
                            <div class="forecast__bg" style="background-image: url('images/forecast__02.jpg')"></div>
                            <div class="forecast__wrap">
                                <div class="forecast__desktop">
                                    <div class="forecast__title">Завершенные прогнозы</div>
                                    <div class="forecast__text">Мы проанализировали для вас каждый прогноз, измерили их точность и доходность</div>
                                </div>
                                <div class="forecast__data">
                                    <table class="table table_collapse forecast__table">
                                        <thead>
                                        <tr>
                                            <th>Аналитик</th>
                                            <th class="text_center">Символ</th>
                                            <th class="text_center">Дата и время <br/>закрытия</th>
                                            <th class="text_center">Направление <br/><small>точность в %</small></th>
                                            <th class="text_center">Прогноз <br/><small>точность в %</small></th>
                                            <th class="text_center">Доходность <br/><small>пункты</small></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Александр</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Дата и время закрытия" class="text_center">сегодня 10:30</td>
                                            <td data-head="Направление" class="text_center">100</td>
                                            <td data-head="Прогноз" class="text_center text_nowrap">34</td>
                                            <td data-head="Доходность" class="text_center text_nowrap">998</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Дата и время закрытия" class="text_center">сегодня 10:30</td>
                                            <td data-head="Направление" class="text_center">100</td>
                                            <td data-head="Прогноз" class="text_center text_nowrap">34</td>
                                            <td data-head="Доходность" class="text_center text_nowrap">998</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Дата и время закрытия" class="text_center">сегодня 10:30</td>
                                            <td data-head="Направление" class="text_center">100</td>
                                            <td data-head="Прогноз" class="text_center text_nowrap">34</td>
                                            <td data-head="Доходность" class="text_center text_nowrap">998</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Дата и время закрытия" class="text_center">сегодня 10:30</td>
                                            <td data-head="Направление" class="text_center">100</td>
                                            <td data-head="Прогноз" class="text_center text_nowrap">34</td>
                                            <td data-head="Доходность" class="text_center text_nowrap">998</td>
                                        </tr>
                                        <tr>
                                            <td data-head="Аналитик"><a href="#"><strong>Vitron</strong></a></td>
                                            <td data-head="Символ" class="text_center text_nowrap"><strong>USD/RUB</strong></td>
                                            <td data-head="Дата и время закрытия" class="text_center">сегодня 10:30</td>
                                            <td data-head="Направление" class="text_center">100</td>
                                            <td data-head="Прогноз" class="text_center text_nowrap">34</td>
                                            <td data-head="Доходность" class="text_center text_nowrap">998</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="text_center">
                                        <a href="#" class="btn btn_arrow_right">
                                            <span>Посмотреть ещё</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="gods_row">
                        <div class="goods goods_sm">
                            <a href="#" class="goods__image">
                                <img src="images/goods__05.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Аналитики</a>
                                <div class="goods__text">Сравните аналитиков по опыту, точности прогнозов и их доходности. Посмотрите профиль и получите более подробную информацию</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="goods goods_sm">
                            <a href="#" class="goods__image">
                                <img src="images/goods__06.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Инвестиционные портфели</a>
                                <div class="goods__text">Выберите портфель от аналитика с учетом точности его прогнозов, срока инвестиций и доходности</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="goods goods_sm goods_long">
                            <a href="#" class="goods__image">
                                <div class="goods__image_sm">
                                    <img src="images/goods__07_sm.jpg" class="img_fluid" alt="">
                                </div>
                                <div class="goods__image_base">
                                    <img src="images/goods__07.jpg" class="img_fluid" alt="">
                                </div>
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Роботы</a>
                                <div class="goods__text">Подключите робота для торговли, используя прогнозы выбранного аналитика</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="gods_row mb_42">
                        <div class="goods">
                            <a href="#" class="goods__image">
                                <img src="images/goods__03.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Обучение</a>
                                <div class="goods__text">Получайте знания о рынке и методах торговли</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="goods">
                            <a href="#" class="goods__image">
                                <img src="images/goods__04.jpg" class="img_fluid" alt="">
                            </a>
                            <div class="goods__content">
                                <a href="#" class="goods__title">Вебинары</a>
                                <div class="goods__text">Принимайте участие в бесплатных вебинарах и подписывайтесь на платные с участием профессиональных трейдеров</div>
                                <div class="goods__button">
                                    <a href="#" class="btn btn_arrow_right">
                                        <span>Подробнее</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 22 12"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__arrow_right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__top">
                        <div class="footer__top_logo">
                            <a class="footer__logo" href="#">
                                <strong>FIN</strong>
                                <span>Prognoz</span>
                            </a>
                        </div>
                        <div class="footer__top_nav">
                        <div class="footer__title">Информация</div>
                            <ul class="footer__nav">
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">О финансовых рынках</a></li>
                                <li><a href="#">Условия пользования</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_phones">
                            <div class="footer__title">Контакты</div>
                            <ul class="footer__phones">
                                <li><a href="tel:88001234567">8 800 1234567</a></li>
                                <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                            </ul>
                        </div>
                        <div class="footer__top_contact">
                            <div class="footer__title">Напишите нам</div>
                            <ul class="footer__contact">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__top_links">
                            <div class="footer__links">
                                <div class="footer__links_col">
                                    <a href="#callback" class="btn btn_blue btn_modal">Заказать звонок</a>
                                </div>
                                <div class="footer__links_col">
                                    <ul class="footer__social">
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer__bottom_col">
                            <span>®FinPrognoz, 2019</span>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Политика конфиденциальности</a>
                        </div>
                        <div class="footer__bottom_col">
                            <a href="#">Договор оферты</a>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="cookies">
                <div class="container">
                    <div class="cookies__wrap">
                        <span class="cookies__close"></span>
                        <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                        <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                        <ul class="btn_group">
                            <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                            <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>

        <!-- Покупка -->
        <div class="hide">
            <a href="#purchase" class="purchase_open btn_modal"></a>
            <div class="purchase" id="purchase">
                <div class="purchase__title">Прогноз</div>
                <div class="purchase__name">Лукойл</div>
                <div class="purchase__date">на 22.11.2019</div>
                <div class="purchase__price">Цена: <strong>490</strong> <span>руб.</span></div>
                <div class="text_center">
                    <button type="button" class="btn btn_blue">Купить</button>
                </div>
            </div>
        </div>
            <!-- -->

        <!-- Thanks -->
        <div class="hide">
            <a href="#thanks" class="thanks_open btn_modal"></a>
            <div class="thanks" id="thanks">
                Сообщение успешно отправлено.<br/>
                В ближайшее время мы свяжемся с вами!
            </div>
        </div>
        <!-- -->

        <!-- Callback -->
        <div class="hide">
            <div class="callback" id="callback">
                <div class="callback__title">Заказать звонок</div>
                <form class="form">
                    <div class="mb_20">
                        <div class="form_elem">
                            <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Имя</span>
                        </div>
                    </div>
                    <ul class="callback__row">
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Телефон" checked>
                                <span>Телефон</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="WhatsApp">
                                <span>WhatsApp</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Viber">
                                <span>Viber	</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Telegram">
                                <span>Telegram</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_radio">
                                <input type="radio" name="type" value="Skype">
                                <span>Skype</span>
                            </label>
                        </li>
                    </ul>
                    <div class="mb_30">
                        <div class="form_elem">
                            <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Телефон</span>
                        </div>
                    </div>
                    <div class="mb_25">
                        <label class="form_checkbox">
                            <input type="checkbox" name="check" checked>
                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                        </label>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_blue">Заказать</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- -->


        <!-- Auth -->
        <div class="hide">
            <div class="auth" id="auth">
                <div class="auth__nav">
                    <a href="#" data-tab=".tab1" class="active">Авторизация</a>
                    <a href="#" data-tab=".tab2">Регистрация</a>
                </div>
                <div class="auth__content">
                    <div class="auth__tab tab1 active">
                        <form class="form">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">E-mail</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <div class="form_elem error">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Пароль</span>
                                    <span class="form_elem__message form_elem__alert">Поле должно быть заполнено</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="check" checked>
                                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="mb_20 text_center">
                                <button type="submit" class="btn btn_blue">Войти</button>
                            </div>
                            <div class="mb_40 text_center">
                                <a href="#"><strong>Забыли пароль?</strong></a>
                            </div>
                        </form>
                        <div class="auth__subtitle"><span>Войти через соц. сети</span></div>
                        <div class="auth__social">
                            <a href="#">
                                <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="auth__tab tab2">
                        <div class="auth__reg">
                            <div class="auth__reg_form">
                                <form class="form">
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="firstName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="email" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Email<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="passwordOne" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <div class="form_elem">
                                            <input type="text" name="passwordTwo" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Подтвердить пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="check" checked>
                                            <span>Я принимаю <a href="#">Правила и условия пользования</a></span>
                                        </label>
                                    </div>
                                    <div class="text_center">
                                        <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                                    </div>
                                </form>
                            </div>
                            <div class="auth__reg_good">
                                <div class="auth__reg_title">Ваша учетная запись ещё не активна. <br/>Подтвердите свой e-mail.</div>
                                <div class="auth__reg_text">Если у вас нет письма в папке Входящие, то проверьте папку Спам или войдите при помощи</div>
                                <div class="auth__social mb_25">
                                    <a href="#">
                                        <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                                    </a>
                                </div>
                                <div class="auth__subtitle mb_25"><span>или</span></div>
                                <button type="button" class="btn">Отправить ссылку активации ещё раз</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- Reg base -->
        <div class="hide">
            <div class="auth" id="reg_base">
                <div class="auth__nav">
                    <a href="#" data-tab=".tab1">Авторизация</a>
                    <a href="#" data-tab=".tab2" class="active">Регистрация</a>
                </div>
                <div class="auth__content">
                    <div class="auth__tab tab1">
                        <form class="form">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">E-mail</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <div class="form_elem error">
                                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Пароль</span>
                                    <span class="form_elem__message form_elem__alert">Поле должно быть заполнено</span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="check" checked>
                                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="mb_20 text_center">
                                <button type="submit" class="btn btn_blue">Войти</button>
                            </div>
                            <div class="mb_40 text_center">
                                <a href="#"><strong>Забыли пароль?</strong></a>
                            </div>
                        </form>
                        <div class="auth__subtitle"><span>Войти через соц. сети</span></div>
                        <div class="auth__social">
                            <a href="#">
                                <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="auth__tab tab2 active">
                        <div class="auth__reg">
                            <div class="auth__reg_form">
                                <form class="form">
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="firstName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="email" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Email<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_10">
                                        <div class="form_elem">
                                            <input type="text" name="passwordOne" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <div class="form_elem">
                                            <input type="text" name="passwordTwo" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                            <span class="form_elem__label">Подтвердить пароль<sup class="color_blue">*</sup></span>
                                        </div>
                                    </div>
                                    <div class="mb_20">
                                        <label class="form_checkbox">
                                            <input type="checkbox" name="check" checked>
                                            <span>Я принимаю <a href="#">Правила и условия пользования</a></span>
                                        </label>
                                    </div>
                                    <div class="text_center">
                                        <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                                    </div>
                                </form>
                            </div>
                            <div class="auth__reg_good">
                                <div class="auth__reg_title">Ваша учетная запись ещё не активна. <br/>Подтвердите свой e-mail.</div>
                                <div class="auth__reg_text">Если у вас нет письма в папке Входящие, то проверьте папку Спам или войдите при помощи</div>
                                <div class="auth__social mb_25">
                                    <a href="#">
                                        <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                                    </a>
                                    <a href="#">
                                        <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                                    </a>
                                </div>
                                <div class="auth__subtitle mb_25"><span>или</span></div>
                                <button type="button" class="btn">Отправить ссылку активации ещё раз</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- Reg -->
        <div class="hide">
            <div class="auth auth_md" id="reg">
                <div class="auth__title">Регистрация аналитиком</div>
                <form class="form">
                    <div class="auth__row">
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="nikName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Ник<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="lastName" class="form_elem__input form_elem__private" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Фамилия<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="auth__row">
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="firstName" class="form_elem__input form_elem__private" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="secondName" class="form_elem__input form_elem__private" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Отчество</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="auth__row mb_15">
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="phone" class="form_elem__input form_elem__private" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Телефон<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="email" class="form_elem__input form_elem__private" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Email</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="auth__label"><span>Удобный способ связи</span></div>
                    <ul class="form_inline mb_20">
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="n1">
                                <span>Whatsapp</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="n1">
                                <span>Viber</span>
                            </label>
                        </li>
                        <li>
                            <label class="form_checkbox">
                                <input type="checkbox" name="n1">
                                <span>Telegram</span>
                            </label>
                        </li>
                    </ul>

                    <div class="auth__row">
                        <div class="auth__col">
                            <div class="gender">
                                <div class="gender__label">Пол:</div>
                                <div class="gender__form">
                                    <div class="mb_08">
                                        <label class="form_radio">
                                            <input type="radio" name="sex" value="Мужской" checked>
                                            <span>Мужской</span>
                                        </label>
                                    </div>
                                    <div class="mb_08">
                                        <label class="form_radio">
                                            <input type="radio" name="sex" value="Женский">
                                            <span>Женский</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="day" class="form_elem__input form_elem__calendar" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Дата рождения</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="auth__row mb_15">
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="country" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Страна</span>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="city" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Город</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="auth__row">
                        <div class="auth__col">
                            <div class="mb_20">
                                <label class="form_label"><span>Опыт на финансовых рынках (лет)</span></label>
                                <div class="select">
                                    <input type="hidden" name="select01" value="С 2000 года">
                                    <div class="select__label">
                                        <span>С 2000 года</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="select__dropdown">
                                        <ul class="select__option">
                                            <li><span>С 2000 года</span></li>
                                            <li><span>С 2004 года</span></li>
                                            <li><span>С 2008 года</span></li>
                                            <li><span>С 2012 года</span></li>
                                            <li><span>С 2016 года</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="auth__col">
                            <div class="mb_20">
                                <label class="form_label"><span>Методы анализа рынка</span></label>
                                <div class="select">
                                    <input type="hidden" name="select02" value="Значение">
                                    <div class="select__label">
                                        <span>Значение</span>
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <div class="select__dropdown">
                                        <ul class="select__list">
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 1</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 2</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 3</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 4</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 5</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="vv">
                                                    <span>Значение 6</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="select__buttons">
                                            <li><button type="button" class="btn btn_green btn_sm select_submit">Выбрать</button></li>
                                            <li><button type="button" class="btn_clear select_clear">Очистить</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb_10">
                        <div class="auth__info">Информация недоступна для других пользователей</div>
                    </div>
                    <div class="mb_20">
                        <label class="form_checkbox">
                            <input type="checkbox" name="check" checked>
                            <span>Я принимаю <a href="#">Правила и условия пользования</a></span>
                        </label>
                    </div>
                    <div class="text_center">
                        <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- -->

        <div class="hide">
            <a href="#subscription" class="subscription_open btn_modal"></a>
            <div class="subscription" id="subscription">
                <div class="subscription__title">Стоимость подписки Эксперт</div>
                <div class="subscription__row">
                    <div class="subscription__item">
                        <div class="subscription__item_period">12 месяцев</div>
                        <div class="subscription__item_price">
                            <strong>290</strong>
                            <span>руб/мес</span>
                        </div>
                        <div>
                            <a href="#" class="btn btn_blue">Подписаться</a>
                        </div>
                    </div>
                    <div class="subscription__item">
                        <div class="subscription__item_period">6 месяцев</div>
                        <div class="subscription__item_price">
                            <strong>390</strong>
                            <span>руб/мес</span>
                        </div>
                        <div>
                            <a href="#" class="btn btn_blue">Подписаться</a>
                        </div>
                    </div>
                    <div class="subscription__item">
                        <div class="subscription__item_period">1 месяц</div>
                        <div class="subscription__item_price">
                            <strong>490</strong>
                            <span>руб/мес</span>
                        </div>
                        <div>
                            <a href="#" class="btn btn_blue">Подписаться</a>
                        </div>
                    </div>
                </div>
                <div class="subscription__info">Нажимая кнопку «Подписаться» и осуществляя покупку, Вы принимаете условия <a href="#">Публичной Оферты</a></div>
            </div>
        </div>

        <div class="hide">
            <a href="#rules" class="rules_open btn_modal"></a>
            <div class="modal_page" id="rules">
                <div class="modal_page__title">Правила и условия размещения прогнозов</div>
                <ol class="modal_page__list">
                    <li>Ваши размещенные прогнозы находятся на странице <a href="#"><strong>Мои прогнозы</strong></a> в <strong>Личном кабинете</strong>.</li>
                    <li>Ваш прогноз будет автоматически завершен в указанную вами дату. Расчет точности и доходности будет проведет по лучшей для вас цене на эту дату.</li>
                    <li>Диапазон отклонения даты - это временной диапазон, равный ¼ времени до и после его завершения начиная с даты открытия прогноза. В этом диапазоне вы можете вручную закрыть прогноз по текущей цене без потери точности и доходности прогноза. В случае автопродления прогноза срок его завершения переносится на новую дату, указанную в этом диапазоне.</li>
                    <li>Автопродление прогноза - это возможность продлить срок прогноза до указанной даты в случае, если цена не достигла указанного уровня в установленный вами срок.</li>
                    <li>S/L - это уровень стоп-лосс. Он означает, что если изменение цены пойдет в другом направлении нежели вы прогнозировали, то вы рекомендуете пользователю закрыть сделку по этой цене.</li>
                    <li>Вы можете Завершить размещенный прогноз вручную раньше установленной вами даты его завершения.</li>
                    <li>Если вы вручную закрываете прогноз, когда с момента его размещения прошло ¾ времени до его завершения, то расчеты точности и доходности будут проведены в соответствии с текущей ценой завершения.</li>
                    <li>Если вы закрываете прогноз, когда с момента его размещения не прошло ¾ времени до его завершения, то расчеты точности и доходности будут равны 0.</li>
                    <li>Вы можете продлить срок прогноза выбрав функцию Автопродление на странице <a href="#"><strong>Мои прогнозы</strong></a> в <strong>Личном кабинете</strong>.</li>
                    <li>Если в установленную дату прогноз не достиг прогнозируемой цены, то прогноз может быть автоматически продлен на срок не более ¼ времени длительности прогноза с даты установки. Расчеты точности и доходности будут проведены по лучшей цена на новую дату или на момент его закрытия вручную.</li>
                    <li><strong class="color_red">Запрещено</strong> размещать повторные прогнозы на одинаковый срок, если цена открытия второго прогноза находится в пределах диапазона цен открытия и закрытия ранее установленного прогноза на этот срок. Пример, в первом прогнозе вы установили цену открытия в 100 рублей и прогнозируемую цену в 150 рублей. Разрешено размещать второй прогноз с любым направлением тренда с ценой открытия от 150 рублей и выше или от 100 рублей и ниже. Цена закрытия может быть любой.</li>
                    <li><strong class="color_red">Запрещено</strong> размещать повторные прогнозы на одинаковый срок и с одинаковым направлением тренда, если цена открытия или закрытия второго прогноза находится в пределах диапазона цен открытия и закрытия ранее установленного прогноза на этот срок. Пример, в первом прогнозе вы установили цену открытия в 100 рублей и прогнозируемую цену в 150 рублей. Разрешено размещать второй прогноз с таким же направлением тренда с ценой открытия от 150 рублей и выше или от 100 рублей и ниже. Цена закрытия второго прогноза также должна быть от 150 р и выше или 100 р и ниже.</li>
                    <li><strong class="color_red">Запрещено</strong> размещать на один и тот же срок взаимоисключающие прогнозы (локированные прогнозы). Пример, в первом прогнозе вы установили цену открытия в 100 рублей и прогнозируемую цену в 150 рублей. Во втором прогнозе вы установили цену открытия в 100 рублей и прогнозируемую цену в 50 рублей.</li>
                    <li>Если у аналитика нет текущих прогнозов и отсутствуют новые прогнозы в течении 30 дней, то аккаунт замораживается и не показывается пользователям.</li>
                    <li><strong class="color_green">Удачи в прогнозах!</strong></li>
                </ol>
            </div>
        </div>

        <div class="hide">
            <a href="#requirement" class="requirement_open btn_modal"></a>
            <div class="modal_page" id="requirement">
                <div class="modal_page__title mb_25">Условия оплаты за прогнозы</div>
                <p>Платим аналитикам за каждый прогноз с точностью цены не менее 60%.</p>
                <p>
                    Возможна градация от <br/>
                    60 - 80% точность - <strong>100 руб.</strong> <br/>
                    80 - 100% точность - <strong>200 руб.</strong>
                </p>
                <p>Точность тренда всех прогнозов за месяц - <strong>не менее 70%.</strong> Если меньше то ни один прогноз не оплачивается.</p>
                <p><strong>Оплата прогнозов с коэффициентом:</strong></p>
                <ul class="modal_page__price">
                    <li>текущий день + 2 дня - коэффициент <strong>0,2</strong></li>
                    <li>от 3 -7 дней - коэффициент <strong>0,4</strong></li>
                    <li>от 8 -15 дней - коэффициент <strong>0,7</strong></li>
                    <li>от 16 - 30 дней - коэффициент <strong>1</strong></li>
                    <li>от мес и более - коэффициент <strong>2</strong></li>
                </ul>
                <p>Расчет оплаты проводим на следующий день по окончании каждого месяца с даты регистрации. Если в месяце нет такого числа дней, то расчеты делаем первого числа следующего месяца.</p>
                <p>В расчет принимаются только завершенные прогнозы.</p>
                <p class="mb_50">Если прогноз был завершен вручную аналитиком раньше времени, то считаем по цене закрытия.</p>

                <h2>Потребность в прогнозах</h2>

                <div class="need">

                    <ul class="need__legend">
                        <li class="need__legend_red">символы с высокой потребностью в прогнозах</li>
                        <li class="need__legend_blue">символы со средней потребностью в прогнозах</li>
                    </ul>

                    <div class="need__table">
                        <div class="need__table_row">
                            <div class="need__table_head">Акции</div>
                            <div class="need__table_col need__table_red">Лукойл</div>
                            <div class="need__table_col need__table_red">Газпром</div>
                            <div class="need__table_col need__table_red">NIKE</div>
                        </div>
                        <div class="need__table_row">
                            <div class="need__table_head">Облигации</div>
                            <div class="need__table_col need__table_red">Евросоюз</div>
                            <div class="need__table_col need__table_blue">Россия</div>
                        </div>
                        <div class="need__table_row">
                            <div class="need__table_head">Валюты</div>
                            <div class="need__table_col need__table_blue">EUR/USD</div>
                            <div class="need__table_col need__table_blue">USD/RUB</div>
                            <div class="need__table_col need__table_blue">EUR/RUB</div>
                        </div>
                        <div class="need__table_row">
                            <div class="need__table_head">Индексы</div>
                        </div>
                        <div class="need__table_row">
                            <div class="need__table_head">Сырье</div>
                        </div>
                        <div class="need__table_row">
                            <div class="need__table_head">Криптовалюты</div>
                        </div>
                    </div>

                </div>

                <div class="need__text mb_20"><sup class="color_blue">*</sup> В данной таблице отсутствуют символы, по которым уже есть прогнозы в достаточном количестве, но это не означает, что вы не можете размещать по ним свои прогнозы. Ваши прогнозы могут быть лучше уже размещенных!</div>


            </div>
        </div>


        <!-- Зарегистрируйтесь аналитиком -->
        <div class="hide">
            <a href="#alert" class="alert_open btn_modal"></a>
            <div class="modal_alert" id="alert">
                <div class="modal_alert__text">Зарегистрируйтесь аналитиком и получите возможность размещать свои прогнозы</div>
                <a href="#" class="btn btn_blue">Зарегистрироваться аналитиком</a>
            </div>
        </div>

        <!-- Ваш прогноз принят -->
        <div class="hide">
            <a href="#good" class="good_open btn_modal"></a>
            <div class="modal_alert" id="good">
                <div class="modal_alert__text mb_20">Ваш прогноз принят</div>
                <a href="#" class="btn btn_blue">ОК</a>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
        <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/vendor/raty.js/jquery.raty.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
