<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
    <link rel="stylesheet" href="js/vendor/datetimepicker/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="css/main.css">

    <style>

    </style>

</head>
<body>

<div class="page page_content">

    <div class="top">
        <div class="container">
            <div class="top__row">
                <div class="top__toggle nav_toggle">
                    <span></span>
                </div>
                <div class="top__rate">
                    <div class="top__rate_item">
                        <div class="top__rate_name">GOLD</div>
                        <div class="top__rate_value top__rate_up">
                            <span>1415,50</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div class="top__rate_item">
                        <div class="top__rate_name">EURUSD</div>
                        <div class="top__rate_value top__rate_up">
                            <span>1,101241</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div class="top__rate_item">
                        <div class="top__rate_name">GBPUSD</div>
                        <div class="top__rate_value top__rate_down">
                            <span>1,28418</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div class="top__rate_item">
                        <div class="top__rate_name">EURUSD</div>
                        <div class="top__rate_value top__rate_up">
                            <span>1,101241</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_up" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div class="top__rate_item">
                        <div class="top__rate_name">GBPUSD</div>
                        <div class="top__rate_value top__rate_down">
                            <span>1,28418</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 10 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                    </div>
                </div>
                <div class="top__content">
                    <div class="lng">
                        <div class="lng__active">
                            <div class="lng__active_icon">
                                <svg class="ico_svg" viewBox="0 0 12 12"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__global" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="lng__active_name">Ru</div>
                            <div class="lng__active_arrow">
                                <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="lng__content">
                            <a href="#" class="lng__item">En</a>
                            <a href="#" class="lng__item">Ru</a>
                            <a href="#" class="lng__item">De</a>
                        </div>
                    </div>
                    <div class="top__content_item hi">
                        <a href="#" class="btn">Разместить прогноз</a>
                    </div>
                    <div class="top__content_item">
                        <a href="#" class="btn btn_blue">Получить прогноз</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <header class="header">
        <div class="container">
            <div class="header__row">
                <a href="#" class="header__logo">
                    <img src="img/logo.svg" class="img_fluid" about="">
                </a>

                <nav class="nav">
                    <span class="nav__close nav_toggle"></span>
                    <ul class="nav__menu">
                        <li>
                            <a href="#">
                                <span>Прогнозы</span>
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <ul>
                                <li><a href="#"><span>Текущие прогнозы</span></a></li>
                                <li><a href="#"><span>Завершенные прогнозы</span></a></li>
                                <li><a href="#"><span>Заказать прогноз</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <span>Аналитики</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Роботы</span>
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 14 9"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <ul>
                                <li><a href="#"><span>Аренда робота</span></a></li>
                                <li><a href="#"><span>50/50</span></a></li>
                                <li><a href="#"><span>Продажа робота</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <span>Вебинары</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Обучение</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav_blue">
                                <span>Для Аналитиков</span>
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 9 6"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__shape" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <b>99</b>
                            </a>
                            <ul>
                                <li><a href="#"><span>Разместить прогноз</span></a></li>
                                <li><a href="#"><span>Заявки на прогнозы</span><b>99</b></a></li>
                                <li><a href="#"><span>Составить портфель инвестова</span></a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <div class="nav__layout nav_toggle"></div>
                <div class="header__auth">
                    <div class="user">
                        <div class="user__label"><span>Александр Шевченко</span></div>
                        <div class="user__dropdown">
                            <ul>
                                <li><a href="#">Ссылка первая</a></li>
                                <li><a href="#">Ссылка вторая</a></li>
                                <li><a href="#">Ссылка третья</a></li>
                                <li><a href="#">Ссылка четвертая</a></li>
                                <li><a href="#">Выйти</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="main">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Главная</a></li>
                <li><span>Правила и документы</span></li>
            </ul>

            <div class="account">
                <div class="account__header">
                    <span>Правила и документы</span>
                    <button type="button" class="account__header_nav">
                        <span></span>
                    </button>
                </div>
                <div class="account__row mb_40">
                    <div class="account__sidebar">
                        <nav class="account_nav">
                            <ul class="account_nav__single">
                                <li><a href="#"><span>Правила и условия пользования</span></a></li>
                                <li><a href="#"><span>Правила для комментариев к прогнозам</span></a></li>
                                <li><a href="#"><span>Политика конфиденциальности</span></a></li>
                                <li><a href="#"><span>Договор оферты</span></a></li>
                                <li><a href="#"><span>Правила размещения прогнозов</span></a></li>
                                <li><a href="#"><span>Условия досрочного завершения прогноза</span></a></li>
                                <li><a href="#"><span>Договор на торговлю роботом</span></a></li>
                                <li class="active"><a href="#"><span>Заголовок документ</span></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="account__content">

                        <div class="account__title">Заголовок документ</div>
                        <div class="content_text">

                            <p>Данный документ представляет собой договор-оферту, в котором с одной стороны выступает ООО «Паркер», в лице Генерального директора Спиридонова Даниила Евгеньевича, действующего на основании Устава, далее по тексту именуемое «Правообладатель», с другой стороны г-н (г-жа) _________________________, акцептующий(ая) настоящее Соглашение, в дальнейшем именуемый(ая) «Пользователь», совместно именуемые как «Стороны», заключившие настоящее Лицензионное Соглашение оферту (далее по тексту: «Оферта», «Соглашение», «настоящее Соглашение») о нижеследующем:</p>
                            <h4 class="box__subtitle mb_10">1. ПРЕДМЕТ СОГЛАШЕНИЯ (Подзаголовок жирным)</h4>
                            <ul class="numeric numeric_padding">
                                <li data-num="1.1."><div>(Нумерованный текст) Правообладатель предоставляет Пользователю неисключительные имущественные права на целевое использование программы для ЭВМ «Торговый Робот» (далее – «Робот»), на оговоренных в настоящем Соглашении условиях.</div></li>
                                <li data-num="1.2."><div>Назначение и способ использования программы полностью изучены Пользователем до акцепта настоящей Оферты.</div></li>
                                <li data-num="1.3."><div>Любое нарушение настоящего Лицензионного Соглашения будет расцениваться, как нарушение авторских прав Правообладателя.</div></li>
                                <li data-num="1.4."><div>Акцепт настоящего Соглашения происходит путем согласия Пользователя с условиями Оферты на веб-сайте Правообладателя</div></li>
                            </ul>
                            <h4 class="box__subtitle mb_10">2. УСТАНОВКА РОБОТА</h4>
                            <ul class="numeric numeric_padding">
                                <li data-num="2.1."><div>Установка Робота происходит путем подключения Торгового счета Пользователя к Роботу. Робот дублирует прогнозы, приходящие от выбранных Аналитиков, на которых подписан Пользователь.</div></li>
                                <li data-num="2.2.">
                                    <div>
                                        <p>Для подключения Робота Пользователь дает представителю Правообладателя логин и пароль от своего Торгового счета, который может быть открыт у любого брокера, однако для торговли должны использоваться только программы “Meta Trader 4” и “Meta Trader 5” (Торговый терминал):</p>
                                        <ul class="list_disk">
                                            <li>(Перечисление) Первое</li>
                                            <li>Второе</li>
                                            <li>Третье</li>
                                        </ul>
                                    </div>
                                </li>
                                <li data-num="2.3."><div>Правообладатель обязуется подключить Робота Пользователю в течение 3 (Трех) дней с момента получения платежа</div></li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <footer class="footer">
        <div class="container">
            <div class="footer__top">
                <div class="footer__top_logo">
                    <a class="footer__logo" href="#">
                        <img src="img/logo.svg" class="img_fluid" about="">
                    </a>
                </div>
                <div class="footer__top_nav">
                    <div class="footer__title">Информация</div>
                    <ul class="footer__nav">
                        <li><a href="#">О нас</a></li>
                        <li><a href="#">О финансовых рынках</a></li>
                        <li><a href="#">Условия пользования</a></li>
                    </ul>
                </div>
                <div class="footer__top_phones">
                    <div class="footer__title">Контакты</div>
                    <ul class="footer__phones">
                        <li><a href="tel:88001234567">8 800 1234567</a></li>
                        <li><a href="tel:+74951234567">+7 495 1234567</a></li>
                    </ul>
                </div>
                <div class="footer__top_contact">
                    <div class="footer__title">Напишите нам</div>
                    <ul class="footer__contact">
                        <li>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__skype" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 448 512"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 496 512"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="ico_svg" viewBox="0 0 550.795 550.795"  xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__envelope" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="footer__top_links">
                    <div class="footer__links">
                        <div class="footer__links_col">
                            <a href="#" class="btn btn_blue">Заказать звонок</a>
                        </div>
                        <div class="footer__links_col">
                            <ul class="footer__social">
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__facebook" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <svg class="ico_svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer__bottom_col">
                    <span>®FinPrognoz, 2019</span>
                </div>
                <div class="footer__bottom_col">
                    <a href="#">Политика конфиденциальности</a>
                </div>
                <div class="footer__bottom_col">
                    <a href="#">Договор оферты</a>
                </div>
            </div>
        </div>
    </footer>

    <div class="cookies">
        <div class="container">
            <div class="cookies__wrap">
                <span class="cookies__close"></span>
                <div class="cookies__title">Пожалуйста, разрешите использование cookies для более эффективной работы с сайтом</div>
                <div class="cookies__text">Мы используем файлы cookie для того, чтобы предоставить Вам больше возможностей при использовании сайта. Файлы cookie представляют собой небольшие фрагменты данных, которые временно сохраняются на вашем компьютере или мобильном устройстве, и обеспечивают более эффективную работу сайта.</div>
                <ul class="btn_group">
                    <li><a href="#" class="btn btn_white btn_shadow">Разрешить</a></li>
                    <li><a href="#" class="btn btn_white_border">Запретить</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>


<!-- Покупка -->
<div class="hide">
    <a href="#purchase" class="purchase_open btn_modal"></a>
    <div class="purchase" id="purchase">
        <div class="purchase__title">Прогноз</div>
        <div class="purchase__name">Лукойл</div>
        <div class="purchase__date">на 22.11.2019</div>
        <div class="purchase__price">Цена: <strong>490 руб.</strong></div>
        <div class="text_center">
            <button type="button" class="btn btn_blue">Купить</button>
        </div>
    </div>
</div>
<!-- -->

<!-- Thanks -->
<div class="hide">
    <a href="#thanks" class="thanks_open btn_modal"></a>
    <div class="thanks" id="thanks">
        Сообщение успешно отправлено.<br/>
        В ближайшее время мы свяжемся с вами!
    </div>
</div>
<!-- -->

<!-- Callback -->
<div class="hide">
    <div class="callback" id="callback">
        <div class="callback__title">Заказать звонок</div>
        <form class="form">
            <div class="mb_20">
                <div class="form_elem">
                    <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                    <span class="form_elem__label">Имя</span>
                </div>
            </div>
            <ul class="callback__row">
                <li>
                    <label class="form_radio">
                        <input type="radio" name="type" value="Телефон" checked>
                        <span>Телефон</span>
                    </label>
                </li>
                <li>
                    <label class="form_radio">
                        <input type="radio" name="type" value="WhatsApp">
                        <span>WhatsApp</span>
                    </label>
                </li>
                <li>
                    <label class="form_radio">
                        <input type="radio" name="type" value="Viber">
                        <span>Viber	</span>
                    </label>
                </li>
                <li>
                    <label class="form_radio">
                        <input type="radio" name="type" value="Telegram">
                        <span>Telegram</span>
                    </label>
                </li>
                <li>
                    <label class="form_radio">
                        <input type="radio" name="type" value="Skype">
                        <span>Skype</span>
                    </label>
                </li>
            </ul>
            <div class="mb_30">
                <div class="form_elem">
                    <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                    <span class="form_elem__label">Телефон</span>
                </div>
            </div>
            <div class="mb_25">
                <label class="form_checkbox">
                    <input type="checkbox" name="check" checked>
                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                </label>
            </div>
            <div class="text_center">
                <button type="submit" class="btn btn_blue">Заказать</button>
            </div>
        </form>
    </div>
</div>
<!-- -->


<!-- Auth -->
<div class="hide">
    <div class="auth" id="auth">
        <div class="auth__nav">
            <a href="#" data-tab=".tab1" class="active">Авторизация</a>
            <a href="#" data-tab=".tab2">Регистрация</a>
        </div>
        <div class="auth__content">
            <div class="auth__tab tab1 active">
                <form class="form">
                    <div class="mb_10">
                        <div class="form_elem">
                            <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">E-mail</span>
                        </div>
                    </div>
                    <div class="mb_20">
                        <div class="form_elem error">
                            <input type="text" name="name" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                            <span class="form_elem__label">Пароль</span>
                            <span class="form_elem__message form_elem__alert">Поле должно быть заполнено</span>
                        </div>
                    </div>
                    <div class="mb_20">
                        <label class="form_checkbox">
                            <input type="checkbox" name="check" checked>
                            <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                        </label>
                    </div>
                    <div class="mb_20 text_center">
                        <button type="submit" class="btn btn_blue">Войти</button>
                    </div>
                    <div class="mb_40 text_center">
                        <a href="#"><strong>Забыли пароль?</strong></a>
                    </div>
                </form>
                <div class="auth__subtitle"><span>Войти через соц. сети</span></div>
                <div class="auth__social">
                    <a href="#">
                        <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                    </a>
                    <a href="#">
                        <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="auth__tab tab2 ">
                <div class="auth__reg done">
                    <div class="auth__reg_form">
                        <form class="form">
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="lastName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Фамилия</span>
                                </div>
                            </div>
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="firstName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Имя<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="secondName" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Отчество</span>
                                </div>
                            </div>
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="email" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Email<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="phone" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Телефон</span>
                                </div>
                            </div>
                            <div class="mb_10">
                                <div class="form_elem">
                                    <input type="text" name="passwordOne" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Пароль<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <div class="form_elem">
                                    <input type="text" name="passwordTwo" class="form_elem__input" placeholder="" alt="" autocomplete="off">
                                    <span class="form_elem__label">Подтвердить пароль<sup class="color_blue">*</sup></span>
                                </div>
                            </div>
                            <div class="mb_20">
                                <label class="form_checkbox">
                                    <input type="checkbox" name="check" checked>
                                    <span>Я принимаю <a href="#">Условия пользования</a> и <a href="#">Политику конфиденциальности</a></span>
                                </label>
                            </div>
                            <div class="text_center">
                                <button type="submit" class="btn btn_blue">Зарегистрироваться</button>
                            </div>
                        </form>
                    </div>
                    <div class="auth__reg_good">
                        <div class="auth__reg_title">Ваша учетная запись ещё не активна. <br/>Подтвердите свой e-mail.</div>
                        <div class="auth__reg_text">Если у вас нет письма в папке Входящие, то проверьте папку Спам или войдите при помощи</div>
                        <div class="auth__social mb_25">
                            <a href="#">
                                <img src="img/social/icon__facebook.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__vk.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__twitter.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__instagram.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__ok.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__google.svg" class="img_fluid" alt="">
                            </a>
                            <a href="#">
                                <img src="img/social/icon__pr.svg" class="img_fluid" alt="">
                            </a>
                        </div>
                        <div class="auth__subtitle mb_25"><span>или</span></div>
                        <button type="button" class="btn">Отправить ссылку активации ещё раз</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="js/vendor/svg4everybody.legacy.min.js"></script>
<script src="js/vendor/jquery.mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/vendor/raty.js/jquery.raty.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
<script src="js/vendor/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
